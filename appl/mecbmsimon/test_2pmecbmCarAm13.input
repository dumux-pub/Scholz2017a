# Parameter file for test case 2pmecbmsimon.
# Everything behind a '#' is a comment.
# Type "./test_mecbmsimon --help" for more information.

[TimeManager]
TEnd                = 1.5e7           # duration of the simulation [s]
DtInitial           = 10            # initial time step size [s]
MaxTimeStepSize     = 1200           # maximum time step size
FreqOutput          = 3000           # frequency of VTK output
WriteRestartFile    = 0             # Boolean. Should restart files be written? (1) Yes (0) No

[Grid]
LowerLeft = 0 0
UpperRight = 4 4
Cells = 4 4

[FluidSystem]
NTemperature         = 200          # [-] number of tabularization entries
NPressure            = 200          # [-] number of tabularization entries
PressureLow          = 1e4          # [Pa] low end for tabularization of fluid properties
PressureHigh         = 3e6          # [Pa] high end for tabularization of fluid properties
TemperatureLow       = 273.15       # [K] low end for tabularization of fluid properties
TemperatureHigh      = 400.00       # [K] high end for tabularization of fluid properties

[Problem]
Name                = mecbmCarAm13   # [-] name for output files
ReservoirPressure   = 5E5           # [Pa] Initial reservoir pressure
reservoirSaturation = 0.7           # [-] Initial saturation
Temperature         = 293.15        # [K] reservoir temperature
InnerPressure       = 5.5E5         # [Pa] Pressure value by Robin 80 psi
InnerLiqSaturation  = 0.2           # [-] liquid saturation at inner boundary
OuterPressure       = 5E5           # [Pa] reservoir boundary pressure
OuterLiqSaturation  = 0.2           # [-] liquid saturation at outer boundary

# *=fixed Literature value *?=estimated ?=questionable

[Initial]
initxwCH4           = 1.125e-8      # [-] initial gas saturation : was initSatGas
initxwH2            = 0             # [-] initial mole fraction Hydrogen (minimum value estimate)
initxwAmendment     = 1.595e-5      # [-] initial mole fraction carbon amendment
initxwAcetate       = 0             # [-] initial mole fraction acetate (minimum value estimate)
initxwRMethyl       = 0             # [-] initial mole fraction methyl (minimum value estimate)
initxwTC            = 0             # [-] initial mole fraction of total inorganic carbon
initPhiCCoal        = 0.0          # [m3 biomass/m3 total] initial volume fraction of convertable coal (100g/l with density of 1250 kg/m3)
initPhiPrimBac      = 1e-5          # [m3 biomass/m3 total] *? e-10...e-4 Initial volume fraction of primary bacteria
initPhiSecBac       = 1e-5          # [m3 biomass/m3 total] *? e-10...e-4 Initial volume fraction of secondary bacteria
initPhiAcetoArch    = 1e-5          # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of acetoclastic archaea
initPhiHydroArch    = 1e-5          # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of hydrogenotroPhic archaea
initPhiMethyArch    = 1e-5          # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of methylotroPhic archaea
initGasSaturation   = 0.01           # [-] initial liquid saturation
Salinity            = 0.0           # [-] Initial salinity
initiallyTwoPhases  = 1             # [-] initial phase presence 1 = both, 0= wPhase

[Injection]
injVolumeflux       = 5.8646e-13     # 5.8646e-9 [m³/s] = 1 [m/d] // Johannes: 1.48333333333333e-7  # =10.3/60/1e6 #	//[m³/s] = [ml/min] /[s/min] /[ml/m³]

[BioCoefficients]         
rhoBiofilm          = 10            # [kg/m3] Dry density of biofilm, value fitted by A. Ebigbo 2012 WRR
K                   = 0.01          # [-] Decay rate for microbes
muPrimBacC          = 0.70          # [d-1] *? 0.3...0.8 Maximum specific growth rate for primary bacteria on coal]
muSecBacC           = 0.7           # [d-1] *? 0.5...1.0 Maximum specific growth rate for secondary bacteria on coal
muSecBacAm          = 2.7           # [d-1] *? 0.5...1.0 +larger than muSecBacC Maximum specific growth rate for secondary bacteria on amendment
muAcetoArch         = 8             # [d-1] * Maximum specific growth rate for acetoclastic archaea
muHydroArch         = 1.66          # [d-1] * Maximum specific growth rate for hydrogenotrophic archaea
muMethyArch         = 2.9           # [d-1] * Maximum specific growth rate for methylotrophic archaea
Kc                  = 500           # [g/L] * Monod half saturation constant for coal
KH2                 = 2.0e-5        # [g/L] * Monod half saturation constant for hydrogen
KAm                 = 2.0           # [g/L] *? 0.6...15 Monod half saturation constant for amendment (estimate ranging from 0.6 ... 15 g/L)
KAc                 = 0.240         # [g/L] * Monod half saturation constant for acetate
KCH3                = 2.8e-3        # [g/L] * Monod half saturation constant for methyl
YPrimBacC           = 0.11          # [-] *? Yield of primary bacteria biomass on coal  //TODO
YSecBacC            = 0.11          # [-] * Yield of secondary bacteria on coal  
YSecBacAm           = 0.6           # [-] *? Yield of secondary bacteria on amendment  
YHydroArchH2        = 1.217         # [-] * Yield of hydrogenotrophic archaea biomass on hydrogen  
YAcetoArchAc        = 0.035         # [-] * Yield of acetoclastic Archaea biomass on acetate  
YMethyArchCH3       = 0.02          # [-] * Yield of methylotrophic archaea biomass on methyl  
YH2C                = 0.002604      # [-] *? 0.1...0.001 Yield of hydrogen from coal  
YAcC                = 0.103449      # [-] *? 0.1...0.001 Yield of acetate from coal  
YH2Am               = 0.00868       # [-] * Yield of hydrogen from amendment  
YAcAm               = 0.34483       # [-] * Yield of acetate from amendment  
YCH3Am              = 0.0           # [-] * Yield of methyl from amendment  
YCH4Ac              = 0.259         # [-] * Yield of methane from acetate  
YCH4H2              = 1.569         # [-] * Yield of methane from hydrogen  
YCH4CH3             = 1.0           # [-] * Yield of methane from methyl  

[SorptionCoefficients]
VCH4                = 0             # [mol/l] or m3/kg? values from "Malgo's table [67]"
bCH4                = 0             # [1/Pa] or maybe use 1/MPa !conversion 1/b=Pl with Pl =167.5 psia
VCO2                = 0             # [mol/l] or m3/kg?
bCO2                = 0             # [1/Pa] !conversion 1/b=Pl with Pl =167.5 psia

[SpatialParams]
SolubilityLimit     = 0.295         # [-] solubility limit of salt in brine
Porosity            = 0.1          # [-] initial porosity taken from Irfans Matlab code 0.1+initial
Permeability        = 1E-7          # [m2] arbitrary value at the moment for Permeability
IrreducibleLiqSat   = 0.1           # [-] irreducible liquid saturation
IrreducibleGasSat   = 1e-5          # [-] irreducible gas saturation
Pentry1             = 500           # [Pa]
BCLambda1           = 2             # [-]

[Vtk]
AddVelocity         = 1             # Add extra information
VtuWritingFreq      = 1             # 1: write a vtu file at every timestep, 2: write a vtu file every second timestep ...

[LinearSolver]
ResidualReduction   = 1e-6          # target reduction of the initial residual

[Implicit]
NumericDifferenceMethod = 1         # partial derivatives of the residual (1: forward, 0: central, -1: backward)

[Newton]
MaxRelativeShift    = 1e-9          # tolerance for the shift criterion

[Output]
#Frequency of restart file, flux and VTK output
FreqRestart         = 1000          # how often restart files are written out
FreqOutput          = 50            # frequency of VTK output
FreqMassOutput      = 2             # frequency of mass and evaporation rate output (Darcy)
FreqFluxOutput      = 1000          # frequency of detailed flux output
FreqVaporFluxOutput = 2             # frequency of summarized flux output


