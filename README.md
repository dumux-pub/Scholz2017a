Summary
=======

This is the DuMuX module containing the code for producing the results
published in:

S. Scholz<br>
Modeling microbially enhanced coal-bed methane production<br>
Master's thesis, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 3/2017.


Installation
============

You can build the module just like any other DUNE module. For building and running the executables, please go to the folders
containing the sources listed below. For the basic dependencies see dune-project.org. Note that you have to have the
BOOST library installed for this module.

The easiest way to install this module is to create a new folder and to execute the file
[installScholz2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2017a/raw/master/installScholz2017a.sh)
in this folder.

```bash
mkdir -p Scholz2017a && cd Scholz2017a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2017a/raw/master/installScholz2017a.sh
sh ./installScholz2017a.sh
```

For a detailed information on installation have a look at the DuMuX installation guide or use the DuMuX handbook, chapter 2.

Installation with Docker 
========================

Create a new folder in your favourite location and download the container startup script to that folder.
```bash
mkdir Scholz2017a
cd Scholz2017a
wget https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2017a/-/raw/master/docker_scholz2017a.sh
```

Open the Docker Container by running
```bash
bash docker_scholz2017a.sh open
```
One may build all executales by running 
```bash
cd Scholz2017a/build-gcc/appl/mecbmsimon
make
```
Applications
============

The results in the master's thesis are obtained by compiling and running the programs from the sources listed below,
which can be found in appl/mecbmsimon/:<br>

test_boxmecbmsimon.cc<br>
test_boxmecbmcolumn.cc<br>
test_boxmecbmadsorption.cc<br>

Output
======

Run the program with the respective source file modifications:

* __test_boxmecbmsimon - MECBM Batch Problem__:
    - __Figure 6.1a - Methane produdction__:
        - use test_2pmecbmCombined1.input and adapt according to Thesis or use test_2pmecbmCombined14.input
        - watch out for Amendment concentration! test_2pmecbmCombined1.input has the 0.5 g/l amendment, while all others have 0.1 g/l amendment (line 40)

    - __Figure 6.2 - Methane production with Coal and Amendment consumption__:
        - use test_2pmecbmCombined1.input, test_2pmecbmCoal1.input and test_2pmecbmCarAm1.input or any other of the same available numbers for differnt fits to the experimental data.

    - __Figure 6.3 and 6.4 - Methane production and substrate consumption for 0.1 g/l Amendment__:
        - adapt inputs as described in Figures 6.1, 6.2 with less amendment (line 40)
     
    - __Figure 6.5 - Bacteria volume fraction__:
        - use inputs from instructions for Figures 6.1 and 6.3 and compare with each other

    - __Figure 6.6 - Adsorption__:
        - add Ad- and Desorption parameters from Table 5.4 to input files ("SorptionCoefficients", lines 83-87)


* __test_boxmecbmcolumn - MECBM Column Problem__:

    - __Figure 6.7 - Amendment transport__:
        - use test_2pmecbmColumn13.input

    - __Figure 6.8  Amendment concentration comparison__:
        - use test_2pmecbmColumn14.input and test_2pmecbmColumn15.input and compare amendment distribution results

    - __Figure 6.9 - Bacteria volume fractions__:
        - use test_2pmecbmColumn14.input and compare bacteria volume fractions along column


* __test_boxmecbmadsorption - Adsorption flush scenario__:

   - __Figure 6.10, 6.11 - Methane Storage term with/without Adsorption__:
        - use test_2padsorptionColumn12.input and evaluate storage results
        - set "SorptionCoefficients" (lines 83-87) to zero for reference scenario without ad- and desorption

Used Versions and Software
==========================
For an overview of the used versions of the DUNE and DuMuX modules, please have a look at
[installScholz2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Scholz2017a/raw/master/installScholz2017a.sh).
