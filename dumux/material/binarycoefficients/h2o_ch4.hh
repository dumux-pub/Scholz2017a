/*****************************************************************************
 *   Copyright (C) 2010 by Andreas Lauser                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Binary coefficients for water and methane.
 */
#ifndef DUMUX_BINARY_COEFF_H2O_CH4_HH
#define DUMUX_BINARY_COEFF_H2O_CH4_HH

#include <dumux/material/binarycoefficients/henryiapws.hh>
#include <dumux/material/binarycoefficients/fullermethod.hh>

#include <dumux/material/components/ch4.hh>
#include <dumux/material/components/h2o.hh>

namespace Dumux
{
namespace BinaryCoeff
{

/*!
 * \brief Binary coefficients for water and methane.
 */
class H2O_CH4
{
public:
    /*!
     * \brief Henry coefficent \f$[N/m^2]\f$  for molecular methane in liquid water.
     *
     * See:
     *
     * IAPWS: "Guideline on the Henry's Constant and Vapor-Liquid
     * Distribution Constant for Gases in H2O and D2O at High
     * Temperatures"
     * http://www.iapws.org/relguide/HenGuide.pdf
     */
    template <class Scalar>
    static Scalar henry(Scalar temperature)
    {
        const Scalar E = 2215.6977;
        const Scalar F = -0.1089;
        const Scalar G = -6.6240;
        const Scalar H = 4.6789;

        return henryIAPWS(E, F, G, H, temperature);
    };

    /*!
     * \brief Binary diffusion coefficent [m^2/s] for molecular water and methane.
     *
     * \copybody fullerMethod()
     */
    template <class Scalar>
    static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
    {
        typedef Dumux::H2O<Scalar> H2O;
        typedef Dumux::CH4<Scalar> CH4;

        // atomic diffusion volumes
        const Scalar SigmaNu[2] = { 13.1 /* H2O */,  25.14 /* CH4, estimated from the increments */ };
        // molar masses [g/mol]
        const Scalar M[2] = { H2O::molarMass()*1e3, CH4::molarMass()*1e3 };

        return fullerMethod(M, SigmaNu, temperature, pressure);
    };

    /*!
     * \brief Diffusion coefficent [m^2/s] for molecular methane in liquid water.
     *
     * The empirical equations for estimating the diffusion
     * coefficient in infinite solution which are presented in Reid,
     * 1987 all show a linear dependency on temperature. We thus
     * simply scale the experimentally obtained diffusion coefficient
     * given in Kobayashi by the temperature.
     *
     * See:
     *
     * R. Reid et al.: "The properties of Gases and Liquids", 4th edition,
     * pp. 599, McGraw-Hill, 1987
     *
     * K. Kobayashi: "Optimization Methods for Multiphase Systems in
     * the Subsurface - Application to Methane Migration in Coal
     * Mining Arenas", PhD thesis, University of Stuttgart, Institute
     * of Hydraulic Engineering (Mitteilungsheft 139), p 58, 2004
     */
    template <class Scalar>
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        const Scalar Texp = 273.15 + 25; // [K]
        const Scalar Dexp = 3.55-9; // [m^2/s]
        return Dexp * temperature/Texp;
    };
};

}
} // end namespace

#endif
