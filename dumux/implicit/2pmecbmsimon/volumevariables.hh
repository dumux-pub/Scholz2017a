// -**- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase, n-component mineralization model.
 */
#ifndef DUMUX_2PMECBM_VOLUME_VARIABLES_HH
#define DUMUX_2PMECBM_VOLUME_VARIABLES_HH

#include <vector>
#include <iostream>

#include <dumux/common/math.hh>
#include <dumux/implicit/model.hh>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/porousmediumflow/2pncmin/implicit/volumevariables.hh>
#include "fluxvariables.hh"
#include <dumux/material/constraintsolvers/computefromreferencephase.hh>
#include <dumux/material/constraintsolvers/miscible2pnccomposition.hh>


#include "properties.hh"
#include "indices.hh"

namespace Dumux
{

/*!
 * \ingroup TwoPMECBMModel
 * \ingroup ImplicitVolumeVariables
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase, n-component model.
 */
template <class TypeTag>
class TwoPMECBMVolumeVariables : public TwoPNCMinVolumeVariables<TypeTag>
{
    typedef TwoPMECBMVolumeVariables<TypeTag> ThisType;
    typedef TwoPNCMinVolumeVariables<TypeTag> ParentType;
    friend class TwoPNCMinVolumeVariables<TypeTag>;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum
    {
        dim = GridView::dimension,
        dimWorld=GridView::dimensionworld,

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases =  GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numMajorComponents = GET_PROP_VALUE(TypeTag, NumMajorComponents),

        // formulations
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        pwsn = TwoPNCFormulation::pwsn,
        pnsw = TwoPNCFormulation::pnsw,

        // phase indices
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        PrimBacPhaseIdx   = FluidSystem::PrimBacPhaseIdx,
        SecBacPhaseIdx    = FluidSystem::SecBacPhaseIdx,
        AcetoArchPhaseIdx = FluidSystem::AcetoArchPhaseIdx,
        HydroArchPhaseIdx = FluidSystem::HydroArchPhaseIdx,
        MethyArchPhaseIdx = FluidSystem::MethyArchPhaseIdx,
        CCoalPhaseIdx     = FluidSystem::CCoalPhaseIdx,

        // component indices
        wCompIdx    = FluidSystem::wCompIdx,
        nCompIdx    = FluidSystem::nCompIdx,
        CH4Idx      = FluidSystem::CH4Idx,
        TCIdx      = FluidSystem::TCIdx,

        // phase presence enums
        nPhaseOnly = Indices::nPhaseOnly,
        wPhaseOnly = Indices::wPhaseOnly,
        bothPhases = Indices::bothPhases,

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        useSalinity = GET_PROP_VALUE(TypeTag, useSalinity)

    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename Grid::ctype CoordScalar;
    typedef Dumux::Miscible2pNCComposition<Scalar, FluidSystem> Miscible2pNCComposition;
    typedef Dumux::ComputeFromReferencePhase<Scalar, FluidSystem> ComputeFromReferencePhase;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };



public:

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {
        ParentType::update(priVars, problem, element, fvGeometry, scvIdx, isOldSol);

        //updating the adsorption values for CH4 and CO2
        Scalar V[numComponents]={};
        Scalar b[numComponents]={};
        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
        {

            if (compIdx == CH4Idx)
//                Standard Langmuir Adsorption formulation
//                {
//                adsorption_[compIdx] = V[compIdx] * b[compIdx] * this->fluidState_.moleFraction(nPhaseIdx, compIdx) * this->fluidState_.pressure(nPhaseIdx) /
//                            (1 + b[compIdx] * this->fluidState_.moleFraction(nPhaseIdx, compIdx)*this->fluidState_.pressure(nPhaseIdx) );
//                }

            // Extended Langmuir (not Gibbs conform!)
            {
            adsorption_[compIdx] = V[compIdx] * b[compIdx] * this->fluidState_.moleFraction(nPhaseIdx, compIdx) * this->fluidState_.pressure(nPhaseIdx)  /
                    (1 + b[compIdx] * this->fluidState_.moleFraction(nPhaseIdx, compIdx) * this->fluidState_.pressure(nPhaseIdx)
                    + b[TCIdx]* this->fluidState_.moleFraction(nPhaseIdx, TCIdx) * this->fluidState_.pressure(nPhaseIdx));
            }

            else if (compIdx == TCIdx)
//                Standard Langmuir Adsorption formulation
//            {
//                adsorption_[compIdx] = V[compIdx] * b[compIdx] * this->fluidState_.moleFraction(nPhaseIdx, compIdx) * this->fluidState_.pressure(nPhaseIdx) /
//                            (1 + b[compIdx] * this->fluidState_.moleFraction(nPhaseIdx, compIdx)*this->fluidState_.pressure(nPhaseIdx) );
//            }

            // Extended Langmuir (not Gibbs conform!)
            {
                adsorption_[compIdx] = V[compIdx] * b[compIdx] * this->fluidState_.moleFraction(nPhaseIdx, compIdx) * this->fluidState_.pressure(nPhaseIdx)  /
                                    (1 + b[compIdx] * this->fluidState_.moleFraction(nPhaseIdx, compIdx) * this->fluidState_.pressure(nPhaseIdx)
                                    + b[CH4Idx]* this->fluidState_.moleFraction(nPhaseIdx, CH4Idx) * this->fluidState_.pressure(nPhaseIdx));
            }

            else
                adsorption_[compIdx] = 0;
        }
    }
    /*!
     * \brief Returns the adsorbed volume \f$\mathrm{a_ads}\f$ in \f$\mathrm{[mol/m^3]}\f$ of a component,
     * which is then added to the n-Phase storage term.
     *
     * \param compIdx The component index
     */
    Scalar adsorption(int compIdx) const
    {
        return adsorption_[compIdx];
    }


protected:
    Scalar adsorption_[numComponents];

private:


    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

};

} // end namespace

#endif
