/*
 * MECBM Reactions file
 *
 *  Created on: 26.10.2016
 *      Author: Irfan & Scholz
 */

#ifndef BIO_MECBM_HH
#define BIO_MECBM_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>
#include <dumux/material/fluidsystems/brinech4mecbm.hh>
#include <dumux/implicit/2pmecbmsimon/properties.hh>


#include <cmath>
#include <iostream>
#include <dumux/common/math.hh>

namespace Dumux
{
/*!
 * \brief The equilibrium chemistry is calculated in this class. The function calculateEquilbriumChemistry is used to
 * control the Newton Solver "newton1D". The chemical functions and derivations are implemented in the private part of
 * class.
 */
template <class TypeTag>
class MECBMReactions
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef MECBMReactions<TypeTag> ThisType;


public:

    MECBMReactions()
{
        try
    {

    K_      = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, K);           // Decay Rate for Microbes
    mupbc_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, muPrimBacC);  // Maximum specific growth rate for Primary Bacteria on Coal
    musbc_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, muSecBacC);   // Maximum specific growth rate for Secondary Bacteria on Coal
    musbam_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, muSecBacAm);  // Maximum specific growth rate for Secondary Bacteria on Amendment
    mua_    = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, muAcetoArch); // Maximum specific growth rate for Acetoclastic Archaea
    muh_    = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, muHydroArch); // Maximum specific growth rate for Hydrogenotrophic Archaea
    mum_    = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, muMethyArch); // Maximum specific growth rate for Methylotrophic Archaea

    Kc_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, Kc);  // Monod Half saturation Constant for Coal
    Kh_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, KH2); // Monod Half saturation Constant for Hydrogen
    KAm_    = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, KAm); // Monod Half saturation Constant for Amendment
    KAc_    = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, KAc); // Monod Half saturation Constant for Acetate
    KCH3_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, KCH3);// Monod Half saturation Constant for Methyl

    Ypbc_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YPrimBacC);    // Yield of Primary Bacteria Biomass on Coal
    Ysbc_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YSecBacC);     // Yield of Secondary Bacteria on Coal
    Ysbam_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YSecBacAm);    // Yield of Secondary Bacteria on Amendment
    YhH2_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YHydroArchH2); // Yield of Hydrogenotrophic Archaea Biomass on Hydrogen
    YaAc_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YAcetoArchAc); // Yield of Acetoclastic Archaea Biomass on Acetate
    YmCH3_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients,YMethyArchCH3); // Yield of Methylotrophic Archaea Biomass on Methyl
    YH2c_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YH2C);         // Yield of Hydrogen from Coal
    YAcc_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YAcC);         // Yield of Acetate from Coal
    YH2Am_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YH2Am);        // Yield of Hydrogen from Amendment
    YAcAm_  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YAcAm);        // Yield of Acetate from Amendment
    YCH3Am_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YCH3Am);       // Yield of Methyl from Amendment
    YCH4Ac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YCH4Ac);       // Yield of CH4 from Acetate
    YCH4H2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YCH4H2);       // Yield of CH4 from Hydrogen
    YCH4CH3_= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, YCH4CH3);      // Yield of CH4 from Methyl



    }
    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1);
    }
}

    static const int wPhaseIdx  = FluidSystem::wPhaseIdx;
    static const int nPhaseIdx  = FluidSystem::nPhaseIdx;
    // solid phase idx and in Fluidsystem as well
    static const int pbPhaseIdx = FluidSystem::PrimBacPhaseIdx;
    static const int sbPhaseIdx = FluidSystem::SecBacPhaseIdx;
    static const int aaPhaseIdx = FluidSystem::AcetoArchPhaseIdx;
    static const int haPhaseIdx = FluidSystem::HydroArchPhaseIdx;
    static const int maPhaseIdx = FluidSystem::MethyArchPhaseIdx;
    static const int ccPhaseIdx = FluidSystem::CCoalPhaseIdx;

    static const int wCompIdx   = FluidSystem::wCompIdx;
    static const int nCompIdx   = FluidSystem::nCompIdx;

    static const int H2OIdx     = FluidSystem::H2OIdx;
    static const int CH4Idx     = FluidSystem::CH4Idx;
    static const int NaClIdx    = FluidSystem::NaClIdx;

    static const int CCoalIdx   = FluidSystem::CCoalIdx;
    static const int PrimBacIdx = FluidSystem::PrimBacIdx;
    static const int SecBacIdx  = FluidSystem::SecBacIdx;
    static const int AcetoIdx   = FluidSystem::AcetoArchIdx;
    static const int HydroIdx   = FluidSystem::HydroArchIdx;
    static const int MethyIdx   = FluidSystem::MethyArchIdx;
    static const int AcetateIdx = FluidSystem::AcetateIdx;
    static const int CarAmIdx   = FluidSystem::AmendmentIdx;
    static const int RMethylIdx = FluidSystem::RMethylIdx;
    static const int HydrogenIdx= FluidSystem::H2Idx;
    static const int TCIdx      = FluidSystem::TCIdx;



    static const int numComponents = FluidSystem::numComponents;
    static const int numPhases = FluidSystem::numPhases;
    static const int numSPhases = FluidSystem::numSPhases;


   void reactionSource(PrimaryVariables &R,
           const VolumeVariables &volVars,
           const Scalar dt)
           {
       //define and compute some parameters for simplicity:
//       Scalar porosity = volVars.porosity();
//       Scalar initialPorosity = volVars.initialPorosity();
//       Scalar Sw  =  volVars.saturation(wPhaseIdx);
//       Scalar temperature = volVars.temperature();
       //
       //TODO adapt comments
       Scalar cAcetate = volVars.moleFraction(wPhaseIdx, AcetateIdx) * volVars.molarDensity(wPhaseIdx) * FluidSystem::molarMass(AcetateIdx);    //[kg_suspended_Biomass/m³_waterphase]
       if(cAcetate < 0)
         cAcetate = 0;
       Scalar cCarAm = volVars.moleFraction(wPhaseIdx, CarAmIdx) * volVars.molarDensity(wPhaseIdx) * FluidSystem::molarMass(CarAmIdx);          //[kg_suspended_Biomass/m³_waterphase]
       if(cCarAm < 0)
           cCarAm = 0;
       Scalar cRMethyl = volVars.moleFraction(wPhaseIdx, RMethylIdx) * volVars.molarDensity(wPhaseIdx) * FluidSystem::molarMass(RMethylIdx);    //[kg_suspended_Biomass/m³_waterphase]
       if(cRMethyl < 0)
         cRMethyl = 0;
       Scalar cHydrogen = volVars.moleFraction(nPhaseIdx, HydrogenIdx) * volVars.molarDensity(nPhaseIdx) * FluidSystem::molarMass(HydrogenIdx); //[kg_suspended_Biomass/m³_waterphase]
       if(cHydrogen < 0)
         cHydrogen = 0;

       //compute biomass growth coefficients and rate
       Scalar masspb_ = volVars.precipitateVolumeFraction(pbPhaseIdx)*volVars.density(pbPhaseIdx); //[kg_pb_Biomass/m³_total] volumetric fraction of primary bacteria attached reversibly
       Scalar masssb_ = volVars.precipitateVolumeFraction(sbPhaseIdx)*volVars.density(sbPhaseIdx); //[kg_sb_Biomass/m³_total] volumetric fraction of secondary bacteria attached reversibly
       Scalar massaa_ = volVars.precipitateVolumeFraction(aaPhaseIdx)*volVars.density(aaPhaseIdx); //[kg_aa_Biomass/m³_total] volumetric fraction of acetoclastic archaea attached reversibly
       Scalar massha_ = volVars.precipitateVolumeFraction(haPhaseIdx)*volVars.density(haPhaseIdx); //[kg_ha_Biomass/m³_total] volumetric fraction of hydrogenotrophic archaea attached reversibly
       Scalar massma_ = volVars.precipitateVolumeFraction(maPhaseIdx)*volVars.density(maPhaseIdx); //[kg_ma_Biomass/m³_total] volumetric fraction of methylotrophic archaea attached reversibly
       Scalar masscc_ = volVars.precipitateVolumeFraction(ccPhaseIdx)*volVars.density(ccPhaseIdx); //[kg_cc_Biomass/m³_total] volumetric fraction convertible coal attached reversibly
       if(masscc_ < 0)
         masscc_ = 0;

       Scalar rgpb = mupbc_ * masscc_ / (Kc_ + masscc_)*masspb_;      //growth rate primary bacteria (MSc Irfan 3.2)
       Scalar rdpb = K_ * masspb_;                                            //decay rate primary bacteria (MSc Irfan 3.3)
       Scalar rgsbc = musbc_ * masscc_ / (Kc_ + masscc_)*masssb_;     //growth rate secondary bacteria on coal (MSc Irfan 3.5)
       Scalar rgsbam= musbam_ * cCarAm / (KAm_ + cCarAm)*masssb_;     //growth rate secondary bacteria on amendment (MSc Irfan 3.6)
       Scalar rgsb= rgsbc + rgsbam;                                           //growth rate secondary bacteria total (MSc Irfan 3.7)
       Scalar rdsb = K_ * masssb_;                                            //decay rate secondary bacteria total (MSc Irfan 3.8)
       Scalar rgaa = mua_ * cAcetate / (KAc_ + cAcetate)*massaa_;     //growth rate acetoclastic archaea (MSc Irfan 3.13)
       Scalar rdaa = K_ * massaa_;                                            //decay rate acetoclastic archaea (MSc Irfan 3.14)
       Scalar rgha = muh_ * cHydrogen / (Kh_ + cHydrogen)*massha_;   //growth rate hydrogenotrophic archaea (MSc Irfan 3.10)
       Scalar rdha = K_ * massha_;                                            //decay rate hydrogenotrophic archaea (MSc Irfan 3.11)
       Scalar rgma = mum_ * cRMethyl / (KCH3_ + cRMethyl)*massma_;     //growth rate methylotrophic archaea (MSc Irfan 3.16)
       Scalar rdma = K_ * massma_;                                            //decay rate methylotrophic archaea (MSc Irfan 3.17)

       //Phase reactions in [kg/(m3*s)]
       //introducing *10 factor to match Irfan's Matlab porosity formulation
       R[wCompIdx]   = 0;      // Source term water
//       R[nCompIdx]   = 0;      // Source term Gas --> substituted by R[CH4Idx]
       R[PrimBacIdx] = (rgpb - rdpb)/86400;
       R[SecBacIdx]  = (rgsb - rdsb)/86400;
       R[AcetoIdx]   = (rgaa - rdaa)/86400;
       R[HydroIdx]   = (rgha - rdha)/86400;
       R[MethyIdx]   = (rgma - rdma)/86400;
       R[CCoalIdx]   = -(rgpb / Ypbc_ + rgsbc / Ysbc_) / 86400 /FluidSystem::molarMass(CCoalIdx); // should be rgpb and rgsbc instead of rgsb!
       // but rgsb is used in Irfan's Matlab model instead of rgsbc, therefore to reproduce the mistake this can be adapted here

       //Component reactions in mol/(m3*s)
       R[AcetateIdx] = ((rgsbc * YAcc_ / Ysbc_) + (rgsbam * YAcAm_ / Ysbam_) + (rgpb * YAcc_ / Ypbc_) - rgaa / YaAc_) /86400 * 10 / FluidSystem::molarMass(AcetateIdx) ;
       R[HydrogenIdx]= ((rgsbc * YH2c_ / Ysbc_) + (rgsbam * YH2Am_ / Ysbam_) + (rgpb * YH2c_ / Ypbc_) - rgha / YhH2_) /86400 * 10 / FluidSystem::molarMass(HydrogenIdx) ;
       R[RMethylIdx] = ((rgsbam * YCH3Am_ / Ysbam_) -rgma / YmCH3_) /86400 * 10 / FluidSystem::molarMass(RMethylIdx);

       //check if RMethyl value not negative
       Scalar storageRM = volVars.molarDensity(wPhaseIdx)
                        *volVars.saturation(wPhaseIdx)
                        *volVars.moleFraction(wPhaseIdx, RMethylIdx)
                        *volVars.porosity();
       Scalar test = storageRM + R[RMethylIdx]*dt;
       if (test < 0)
         R[RMethylIdx] = -storageRM/dt*0.8; // was 0.99 but might be too hard

       R[CarAmIdx]   = (-rgsbam / Ysbam_) / FluidSystem::molarMass(CarAmIdx)/86400;
       R[CH4Idx]     = (rgha * YCH4H2_ / YhH2_ + rgaa * YCH4Ac_ / YaAc_  + rgma * YCH4CH3_ / YmCH3_) /86400 * 10 / FluidSystem::molarMass(CH4Idx) ; //+ sorp; //TODO: add when above is solved/clear
       R[TCIdx]      = (rgaa * YCH4Ac_ / YaAc_                          // produce 1 CO2 per 1CH4
                       + (rgma * YCH4CH3_ / YmCH3_)/3                   // produce 1 CO2 per 3 CH4
                       -(rgha * YCH4H2_ / YhH2_) )                      // consume 1 CO2 per 1 CH4
                       /86400 * 10 / FluidSystem::molarMass(TCIdx) ;
//
//               (rgma / YmCH3_ )/ 3 / 86400 * 10 / FluidSystem::molarMass(TCIdx)  //TODO use actual correct values for all pathways: here 1xCO2 per 3xCH4
//                     + (rgaa / YaAc_ ) / 86400 * 10/ FluidSystem::molarMass(TCIdx)  // 1xCO2 per 1xCH4
//                     + (rdpb + rdsb + rdaa + rdha + rdma )/ 10 / 86400 * 10 / FluidSystem::molarMass(TCIdx)  //CO2 from Decay TODO + CO2 from XY? - don't know!
//                     - (rgha / YhH2_ ) / 86400 * 10 / FluidSystem::molarMass(TCIdx) ; // using 1xCO2 to produce 1xCH4


//     std::cout << R[PrimBacIdx] << "DebugRktPrimBac" << std::endl;
//     std::cout << R[SecBacIdx] << "DebugRktSecBac" << std::endl;
//     std::cout << R[AcetoIdx] << "DebugRktAceto" << std::endl;
//     std::cout << R[HydroIdx] << "DebugRktHydro" << std::endl;
//     std::cout << R[MethyIdx] << "DebugRktMethy" << std::endl;
//     std::cout << R[AcetateIdx] << "DebugRktAcetate" << std::endl;
//     std::cout << R[HydrogenIdx] << "DebugRktHydrogen" << std::endl;
//     std::cout << R[RMethylIdx] << "DebugRktRMethyl" << std::endl;
//     std::cout << R[CarAmIdx] << "DebugRktCarAm" << std::endl;
//     std::cout << R[CH4Idx] << "DebugRktCH4" << std::endl;
//     std::cout << R[CCoalIdx] << "DebugRktCCoal" << std::endl;
//     std::cout << rgsbc << "DebugRkt SecBacCoal" << std::endl;
//     std::cout << rgsbam << "DebugRktr SecBacAmend" << std::endl;
//     std::cout << rgma << "DebugRktrgma" << std::endl;
           }


private:
   Scalar K_;

   Scalar mupbc_;
   Scalar musbc_;
   Scalar musbam_ ;
   Scalar mua_ ;
   Scalar muh_;
   Scalar mum_ ;

   Scalar Kc_ ;
   Scalar Kh_ ;
   Scalar KAm_ ;
   Scalar KAc_ ;
   Scalar KCH3_;
   Scalar kAdsCH4_;
   Scalar kDesCH4_;

   Scalar Ypbc_ ;
   Scalar Ysbc_ ;
   Scalar Ysbam_ ;
   Scalar YhH2_ ;
   Scalar YaAc_ ;
   Scalar YmCH3_ ;
   Scalar YH2c_ ;
   Scalar YAcc_;
   Scalar YH2Am_ ;
   Scalar YAcAm_ ;
   Scalar YCH3Am_;
   Scalar YCH4Ac_ ;
   Scalar YCH4H2_ ;
   Scalar YCH4CH3_;




    Scalar pressure_;
    Scalar temperature_;

public:

    Scalar K()       {return K_; }
    Scalar mupbc()   {return mupbc_; }
    Scalar musbc()   {return musbc_; }
    Scalar musbam()  {return musbam_; }
    Scalar mua()     {return mua_; }
    Scalar muh()     {return muh_; }
    Scalar mum()     {return mum_; }

    Scalar Kh()      {return Kh_; }
    Scalar Kc()      {return Kc_; }
    Scalar KAm()     {return KAm_; }
    Scalar KAc()     {return KAc_; }
    Scalar KCH3()    {return KCH3_; }

    Scalar Ypbc()    {return Ypbc_; }
    Scalar Ysbc()    {return Ysbc_; }
    Scalar Ysbam()   {return Ysbam_; }
    Scalar YhH2()    {return YhH2_; }
    Scalar YaAc()    {return YaAc_; }
    Scalar YmCH3()   {return YmCH3_; }
    Scalar YH2c()    {return YH2c_; }
    Scalar YAcc()    {return YAcc_; }
    Scalar YH2Am()   {return YH2Am_; }
    Scalar YAcAm()   {return YAcAm_; }
    Scalar YCH3Am()  {return YCH3Am_; }
    Scalar YCH4Ac()  {return YCH4Ac_; }
    Scalar YCH4H2()  {return YCH4H2_; }
    Scalar YCH4CH3() {return YCH4CH3_; }


// public:

};

} // end namespace

#endif
