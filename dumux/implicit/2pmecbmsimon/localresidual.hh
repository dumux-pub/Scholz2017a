// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the two-phase n-component mineralisation box model.
 */

#ifndef DUMUX_2PMECBM_LOCAL_RESIDUAL_BASE_HH
#define DUMUX_2PMECBM_LOCAL_RESIDUAL_BASE_HH

#include "properties.hh"
#include "fluxvariables.hh"
#include <dumux/porousmediumflow/2pncmin/implicit/localresidual.hh>


namespace Dumux
{
/*!
 * \ingroup TwoPMECBMModel
 * \ingroup ImplicitLocalResidual
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the two-phase n-component mineralization fully implicit model.
 *
 * This class is used to fill the gaps in ImplicitLocalResidual for the two-phase n-component flow.
 */
template<class TypeTag>
class TwoPMECBMLocalResidual: public TwoPNCMinLocalResidual<TypeTag>
{
protected:
    typedef TwoPMECBMLocalResidual<TypeTag> ThisType;
    typedef TwoPNCMinLocalResidual<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, ElementSolutionVector) ElementSolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) Implementation;

    typedef typename GridView::IntersectionIterator IntersectionIterator;


    enum
    {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases = GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),

        replaceCompEqIdx = GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx),

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

        conti0EqIdx = Indices::conti0EqIdx,

        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        pwsn = TwoPNCFormulation::pwsn,
        pnsw = TwoPNCFormulation::pnsw,
        formulation = GET_PROP_VALUE(TypeTag, Formulation)
    };

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef typename GridView::template Codim<0>::Entity Element;

public:

    /*!
     * \brief Evaluate the storage term of the current solution in a
     *        single phase.
     *
     * \param element The element
     * \param phaseIdx The index of the fluid phase
     */
    void evalPhaseStorage(const Element &element, int phaseIdx)
    {
        FVElementGeometry fvGeometry;
        fvGeometry.update(this->gridView_(), element);
        ElementBoundaryTypes bcTypes;
        bcTypes.update(this->problem_(), element, fvGeometry);
        ElementVolumeVariables volVars;
        volVars.update(this->problem_(), element, fvGeometry, false);

        this->residual_.resize(fvGeometry.numScv);
        this->residual_ = 0;

        this->elemPtr_ = &element;
        this->fvElemGeomPtr_ = &fvGeometry;
        this->bcTypesPtr_ = &bcTypes;
        this->prevVolVarsPtr_ = 0;
        this->curVolVarsPtr_ = &volVars;
        evalPhaseStorage_(phaseIdx);
    }
    /*!
     * \brief Evaluate the storage term of the current solution in a
     *        single phase for each component.
     *
     * \param element The element
     * \param phaseIdx The index of the fluid phase
     * \param compIdx The index of the component
     */
    void evalCompStorage(const Element &element, int phaseIdx, int compIdx)
    {
        FVElementGeometry fvGeometry;
        fvGeometry.update(this->gridView_(), element);
        ElementBoundaryTypes bcTypes;
        bcTypes.update(this->problem_(), element, fvGeometry);
        ElementVolumeVariables volVars;
        volVars.update(this->problem_(), element, fvGeometry, false);

        this->residual_.resize(fvGeometry.numScv);
        this->residual_ = 0;

        this->elemPtr_ = &element;
        this->fvElemGeomPtr_ = &fvGeometry;
        this->bcTypesPtr_ = &bcTypes;
        this->prevVolVarsPtr_ = 0;
        this->curVolVarsPtr_ = &volVars;
        evalCompStorage_(phaseIdx, compIdx);
    }
    /*!
     * \brief Evaluate the amount all conservation quantities
     *        (e.g. phase mass) within a sub-control volume.
     *
     * The result should be averaged over the volume (e.g. phase mass
     * inside a sub control volume divided by the volume).
     * In contrast to the 2pnc model, here, the storage of solid phases is included too.
     *
     *  \param storage the mass of the component within the sub-control volume
     *  \param scvIdx The SCV (sub-control-volume) index
     *  \param usePrevSol Evaluate function with solution of current or previous time step
     */
    void computeStorage(PrimaryVariables &storage, int scvIdx, bool usePrevSol) const
    {
        //call parenttype function
        ParentType::computeStorage(storage, scvIdx, usePrevSol);

        const auto& elemVolVars = usePrevSol ? this->prevVolVars_() : this->curVolVars_();
        const VolumeVariables &volVars = elemVolVars[scvIdx];


        //Compute storage term for adsorbed phases

        for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            if (phaseIdx == nPhaseIdx)
            {
                for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    int eqIdx = conti0EqIdx + compIdx;
                    if (replaceCompEqIdx != eqIdx)
                    {
                        storage[eqIdx] += volVars.molarDensity(phaseIdx)
                                          * volVars.saturation(phaseIdx)
                                          * volVars.moleFraction(phaseIdx, compIdx)
                                          * volVars.porosity()
                                          + volVars.adsorption(compIdx);
                    }
//                    {
//                        storage[eqIdx] += volVars.adsorption(compIdx);
//                    }
//                    else
//                    {
//                        storage[replaceCompEqIdx] += volVars.molarDensity(phaseIdx)
//                                                     * volVars.saturation(phaseIdx)
//                                                     * volVars.porosity();
//                    }
                }
//                std::cout<< "Storage gas" << storage << std::endl;
            }
        }
        Valgrind::CheckDefined(storage);
    }


    /*!
     * \copydoc 2pncMin::evalOutflowSegment
     */
    void evalOutflowSegment(const IntersectionIterator &isIt,
                            const int scvIdx,
                            const int boundaryFaceIdx)
    {
        const BoundaryTypes &bcTypes = this->bcTypes_(scvIdx);

        // deal with outflow boundaries
        if (bcTypes.hasOutflow())
        {
            PrimaryVariables values(0.0);
//            asImp_()->computeFlux(values, boundaryFaceIdx, true);
            asImp_().computeFlux(values, boundaryFaceIdx, true);
            Valgrind::CheckDefined(values);

            for (int eqIdx = 0; eqIdx < numEq; ++eqIdx)
            {
                if (!bcTypes.isOutflow(eqIdx) )
                    continue;
                this->residual_[scvIdx][eqIdx] += values[eqIdx];
            }
        }
    }

protected:

    void evalPhaseStorage_(int phaseIdx)
    {
        // evaluate the storage terms of a single phase
        for (int scvIdx = 0; scvIdx < this->fvGeometry_().numScv; scvIdx++)
        {
            PrimaryVariables &result = this->residual_[scvIdx];
            const ElementVolumeVariables &elemVolVars = this->curVolVars_();
            const VolumeVariables &volVars = elemVolVars[scvIdx];

            // compute storage term of all fluid components within all phases
            result = 0;
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                result[conti0EqIdx + compIdx] += volVars.density(phaseIdx)
                                                    * volVars.saturation(phaseIdx)
                                                    * volVars.massFraction(phaseIdx, compIdx)
                                                    * volVars.porosity();
            }
            result *= this->fvGeometry_().subContVol[scvIdx].volume;
        }
    }

    void evalCompStorage_(int phaseIdx, int compIdx)
    {
        // evaluate the storage terms of a single phase
        for (int scvIdx = 0; scvIdx < this->fvGeometry_().numScv; scvIdx++)
        {
            PrimaryVariables &result = this->residual_[scvIdx];
            const ElementVolumeVariables &elemVolVars = this->curVolVars_();
            const VolumeVariables &volVars = elemVolVars[scvIdx];

            // compute storage term of all fluid components within all phases
            result = 0;
            for (int phaseIdx =0; phaseIdx < numPhases; ++phaseIdx)
                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    result[phaseIdx][conti0EqIdx + compIdx] = volVars.density(phaseIdx)
                                                    * volVars.saturation(phaseIdx)
                                                    * volVars.massFraction(phaseIdx, compIdx)
                                                    * volVars.porosity();
                }
            result *= this->fvGeometry_().subContVol[scvIdx].volume;
        }
    }

    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }

};

} // end namespace

#endif
