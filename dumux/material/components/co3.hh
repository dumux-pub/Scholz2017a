/*
 * CO3.hh
 *
 *  Created on: 14.03.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the CO3 fluid properties
 */
#ifndef DUMUX_CO3_HH
#define DUMUX_CO3_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the CO3 fluid properties
 */
template <class Scalar>
class CO3 : public Component<Scalar, CO3<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the CO3.
     */
    static const char *name()
    { return "CO3"; }

    /*!
     * \brief The mass in [kg] of one mole of CO3.
     */
    static Scalar molarMass()
    { return 60.0092e-3; } // kg/mol

    /*!
     * \brief The diffusion Coefficient of CO3 in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2e-9; }

    static Scalar charge()
    {
        return -2.0;
    }

    /*!
     * \brief Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
     */

    static Scalar ai()
    {
        return 5.4e-10;
    }

    /*!
     * \brief Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
     */
    static Scalar bi()
    {
        return 0.0;
    }

};

} // end namespace

#endif

