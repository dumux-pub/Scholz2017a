// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Problem where water is injected in a for flushing precipitated salt clogging a gas reservoir.
 */
#ifndef DUMUX_MECBMSIMON_PROBLEM_HH
#define DUMUX_MECBMSIMON_PROBLEM_HH

#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/material/fluidsystems/brinech4test.hh>
#include <dumux/material/chemistry/biogeochemistry/mecbmsimonreactions.hh>   // Microbes and some equations are hear
#include <dumux/implicit/2pmecbmsimon/model.hh>

#include "mecbmsimonspatialparams.hh"
#include "mecbmco2tables.hh"

namespace Dumux
{

template <class TypeTag>
class MECBMSimonProblem;

namespace Properties
{
NEW_TYPE_TAG(MECBMSimonProblem, INHERITS_FROM(BoxTwoPNCMin, MECBMSimonSpatialparams));
NEW_TYPE_TAG(MECBMSimonBoxProblem, INHERITS_FROM(BoxModel, MECBMSimonProblem));
NEW_TYPE_TAG(MECBMSimonCCProblem, INHERITS_FROM(CCModel, MECBMSimonProblem));

// Set the grid type
SET_TYPE_PROP(MECBMSimonProblem, Grid, Dune::YaspGrid<2>);

// Set the problem property
SET_TYPE_PROP(MECBMSimonProblem, Problem, MECBMSimonProblem<TypeTag>);

// Set the CO2 tables used
SET_TYPE_PROP(MECBMSimonProblem, CO2Tables, Dumux::MECBMSimon::CO2Tables);

// Set fluid configuration
SET_PROP(MECBMSimonProblem, Chemistry)
{
    typedef Dumux::MECBMSimonReactions<TypeTag> type;
};
SET_PROP(MECBMSimonProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::MECBMSimon::CO2Tables CO2Tables;
    typedef FluidSystems::BrineCH4<TypeTag, Scalar, CO2Tables, H2O<Scalar>, true/*useComplexrelations=*/> type;
};

// Set the spatial parameters
SET_TYPE_PROP(MECBMSimonProblem, SpatialParams, MECBMSimonSpatialparams<TypeTag>);

// Enable gravity
SET_BOOL_PROP(MECBMSimonProblem, ProblemEnableGravity, false);

//Set properties here to override the default property settings in the model.
SET_INT_PROP(MECBMSimonProblem, ReplaceCompEqIdx, 1);
SET_INT_PROP(MECBMSimonProblem, Formulation, TwoPNCFormulation::pgSl);
}

/*!
 * \ingroup TwoPMECBMSimonModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where water with nutrients is inteceted in a gas reservoir and then microbially enhanced coal bed methane production starts.
 *
 * The domain is sized XYm times YXm and contains an extraction well.
 * TODO: Adapt more!
 * To flush this precipitated salt, water is injected through the gas extraction well in order to dissolve the precipitated salt increasing the permeability and thereby achieving high gas extraction rates later. Here, the system is assumed to be isothermal.
 * Neumann no-flow boundary condition is applied at the top and bottom boundary and Dirichlet boundary condition is used on the right and left sides.
 * The injected water phase migrates downwards due to increase in density as the precipitated salt dissolves.
 *
 * The model uses mole fractions of dissolved components and volume fractions of precipitated salt as primary variables. Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref TwoPNCMinModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_boxmecbmsimon</tt>
 */
template <class TypeTag>
class MECBMSimonProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    enum {

        pressureIdx     = Indices::pressureIdx,
        switchIdx       = Indices::switchIdx,      //Saturation ?

        xwAcetateIdx    = FluidSystem::AcetateIdx,
        xwAmendmentIdx  = FluidSystem::AmendmentIdx,
        xwRMethylIdx    = FluidSystem::RMethylIdx,
        xnH2Idx         = FluidSystem::H2Idx,
        xnCH4Idx        = FluidSystem::CH4Idx,
        xwNaClIdx       = FluidSystem::NaClIdx,
        phiCCoalIdx     = FluidSystem::numComponents +5,//FluidSystem::CCoalIdx
        phiPrimBacIdx   = FluidSystem::numComponents,//FluidSystem::PrimeBacIdx,
        phiSecBacIdx    = FluidSystem::numComponents +1,//FluidSystem::SecBacIdx,
        phiAcetoArchIdx = FluidSystem::numComponents +2,//FluidSystem::AcetoIdx,
        phiHydroArchIdx = FluidSystem::numComponents +3,//FluidSystem::HydroIdx,
        phiMethyArchIdx = FluidSystem::numComponents +4,//FluidSystem::MethyIdx,

        //Indices of the components
        wCompIdx     = FluidSystem::BrineIdx,
        nCompIdx     = FluidSystem::CH4Idx,
        AcetateIdx   = FluidSystem::AcetateIdx,
        AmendmentIdx = FluidSystem::AmendmentIdx,
        RMethylIdx   = FluidSystem::RMethylIdx,
        H2Idx        = FluidSystem::H2Idx,
        TCIdx        = FluidSystem::TCIdx,
//        HCO3Idx      = FluidSystem::HCO3Idx,
//        CO3Idx       = FluidSystem::CO3Idx,
        //Indices of the phases
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
//        sPhaseIdx = FluidSystem::sPhaseIdx,

        //Index of the primary component of G and L phase
//         conti0EqIdx = Indices::conti0EqIdx,
//         contiTotalMassIdx = conti0EqIdx + FluidSystem::CH4Idx,
//         precipBiofilmEqIdx = Indices::conti0EqIdx + FluidSystem::numComponents,
//         contiWEqIdx = conti0EqIdx + FluidSystem::BrineIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    MECBMSimonProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        Dune::FMatrixPrecision<>::set_singular_limit(1e-35);

        initxnCH4_              = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxnCH4);
        initxwAcetate_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwAcetate);
        initxwAmendment_        = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwAmendment);
        initxwRMethyl_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwRMethyl);
        initxnH2_               = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxnH2);
        initPhiCCoal_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiCCoal);
        initPhiPrimBac_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiPrimBac);
        initPhiSecBac_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiSecBac);
        initPhiAcetoArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiAcetoArch);
        initPhiHydroArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiHydroArch);
        initPhiMethyArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiMethyArch);
        initLiqSaturation_      = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initLiquidSaturation);
        Salinity_               = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, Salinity);

        reservoirPressure_      = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, ReservoirPressure);
        reservoirSaturation_    = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, reservoirSaturation);
        temperature_            = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Temperature);
        innerPressure_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InnerPressure);
        outerPressure_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OuterPressure);
        outerLiqSaturation_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OuterLiqSaturation);
        innerLiqSaturation_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InnerLiqSaturation);


        nTemperature_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FluidSystem, NTemperature);
        nPressure_              = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FluidSystem, NPressure);
        pressureLow_            = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, PressureLow);
        pressureHigh_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, PressureHigh);
        temperatureLow_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, TemperatureLow);
        temperatureHigh_        = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, TemperatureHigh);
        name_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        freqMassOutput_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqMassOutput);
        storageLastTimestep_    = 0.0;
        lastMassOutputTime_     = 0.0;

        outfile.open("evaporation.out");
        outfile << "time; evaporationRate" << std::endl;

        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);
    }

    ~MECBMSimonProblem()
    {
        outfile.close();
    }

    bool shouldWriteOutput() const
    {
        return this->timeManager().timeStepIndex() % 1 == 0 ||
               this->timeManager().episodeWillBeOver() ||
               this->timeManager().willBeFinished();
    }


    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; }

    /*!
     * \name Boundary conditions
     */

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition &globalPos) const
    {
        const Scalar rMax = this->bBoxMax()[0]; // outerRadius_;
        const Scalar rMin = this->bBoxMin()[0];

        // default to Neumann
        bcTypes.setAllNeumann();
/*
        // Constant pressure  at reservoir boundary (Dirichlet condition)
        if(globalPos[0] > rMax - eps_)
            bcTypes.setAllDirichlet();*/

        // Constant pressure at well (Dirichlet condition)
/*        if(globalPos[0] < rMin + eps_)
            bcTypes.setAllDirichlet();
            */
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
/*    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        const Scalar rMax = this->bBoxMax()[0];
        const Scalar rMin = this->bBoxMin()[0];

        if(globalPos[0] > rMax - eps_)
        {
            values[pressureIdx]   = outerPressure_ ; // Outer boundary pressure bar
            values[switchIdx]     = outerLiqSaturation_; // Saturation outer boundary
            values[phiBiofilmIdx]     = massTomoleFrac_(Salinity_);// mole fraction salt
            values[precipBiofilmIdx] = 0.0;// precipitated salt
        }

        if(globalPos[0] < rMin + eps_)
        {

            values[pressureIdx]      = innerPressure_ ;         // Inner boundary pressure bar
            values[switchIdx]        = innerLiqSaturation_;     // Saturation inner boundary
            values[phiAcetateIdx]     = 0;           // Initial concentration of Acetate
            values[phiAmendmentIdx]   = 0;         // Initial concentration of Amendment
            values[phiRMethylIdx]      = 0;            // Initial concentration of Methyl
            values[PhiH2Idx]          = 0;                // Initial concentration of Hydrogen
            values[precipBiofilmIdx] = 0.0;                     // precipitated salt?

        }
    }*/

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 int scvIdx,
                 int boundaryFaceIdx) const
    {
        values = 0.0;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume due to chemical reactions.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
//TODO: source needed for compiling, but why?
      void source(PrimaryVariables &q,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                  int scvIdx) const
      {
          q = 0;
      }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values[pressureIdx]   = reservoirPressure_;
        values[switchIdx]     = initLiqSaturation_;              // Sl primary variable
        values[xnCH4Idx]      = initxnCH4_;                 // Initial mole fraction of Methane/CH4
        values[xwAmendmentIdx]= initxwAmendment_;           // Initial mole fraction of Amendment
        values[xwAcetateIdx]  = initxwAcetate_;             // Initial mole fraction of Acetate
        values[xwRMethylIdx]  = initxwRMethyl_;             // Initial mole fraction of Methyl
        values[xnH2Idx]       = initxnH2_;                  // Initial mole fraction of Hydrogen
        values[phiCCoalIdx]   = initPhiCCoal_;              // Initial volume fraction
        values[phiPrimBacIdx] = initPhiPrimBac_;            // Initial volume fraction
        values[phiSecBacIdx]  = initPhiSecBac_;             // Initial volume fraction
        values[phiAcetoArchIdx] = initPhiAcetoArch_;        // Initial volume fraction
        values[phiMethyArchIdx] = initPhiMethyArch_;        // Initial volume fraction
        values[phiHydroArchIdx] = initPhiHydroArch_ ;       // Initial volume fraction
        values[xwNaClIdx]     = massTomoleFrac_(Salinity_); // mole fraction
//        std::cout << values[xwNaClIdx] << "DebugProb2"<< std::endl;

       /* if (globalPos[0] > 2.0)
            values[precipBiofilmIdx] = initPrecipitatedSalt1_; // [kg/m^3]
            values[phiCCoalIdx]  = massTomoleFrac_(Salinity_);     // mole fraction
            values[phiAcetateIdx]  = initCAcetate_;             // Initial concentration of Acetate;
            values[phiAmendmentIdx]= initCAmendment_;           // Initial concentration of Amendment;
            values[phiMethylIdx]   = initCMethyl_;              // Initial concentration of Methyl;
            values[phiH2Idx]       = initCH2_;                  // Initial concentration of Hydrogen; */
           /* else
            values[precipBiofilmIdx] = initPrecipitatedSalt1_; // [kg/m^3]*/
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void solDependentSource(PrimaryVariables &source,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            int scvIdx,
                            const ElementVolumeVariables &elemVolVars) const
    {
        source = 0;
        const auto &volVars = elemVolVars[scvIdx];

//        std::cout << source << "DebugProb1" << std::endl;

        Scalar dt = this->timeManager().timeStepSize();

        Chemistry chemistry;
        chemistry.reactionSource(source,
                volVars,
                dt);

//        std::cout << source << "DebugProb2"<< std::endl;

//        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
//               if(globalPos[0]<eps_)
//               {
//                   std::cout<< "moleFractions\n"
//                           << volVars.moleFraction(0,0) << "Brine \n" //moleFraction(phaseIdx,compIdx)
//                           << volVars.moleFraction(0,1) << "Gas CH4 \n"
//                           << volVars.moleFraction(0,2) << "Acetate \n"
//                           << volVars.moleFraction(0,3) << "Amendment \n"
//                           << volVars.moleFraction(0,4) << "RMethyl \n"
//                           << volVars.moleFraction(0,5) << "H2 \n"
//                           << volVars.moleFraction(0,6) << "NaCl \n"
//
//                           << "sum of liquid moleFractions = " <<volVars.moleFraction(0,0)
//                           +volVars.moleFraction(0,1)+volVars.moleFraction(0,2)
//                           +volVars.moleFraction(0,3)+volVars.moleFraction(0,4)
//                           +volVars.moleFraction(0,5)+volVars.moleFraction(0,6)<<"\n"
//                           << volVars.moleFraction(1,0) << "Gas Phase Brine \n"
//                           << volVars.moleFraction(1,1) << "Gas Phase Gas CH4 \n"
//                           << volVars.moleFraction(1,2) << "Gas Phase Acetate \n"
//                           << volVars.moleFraction(1,3) << "Gas Phase Amendment \n"
//                           << volVars.moleFraction(1,4) << "Gas Phase RMethyl \n"
//                           << volVars.moleFraction(1,5) << "Gas Phase H2 \n"
//                           << volVars.moleFraction(1,6) << "Gas Phase NaCl \n"
//                           << "sum of gas moleFractions = " <<volVars.moleFraction(1,0)
//                           +volVars.moleFraction(1,1)+volVars.moleFraction(1,2)
//                           +volVars.moleFraction(1,3)+volVars.moleFraction(1,4)
//                           +volVars.moleFraction(1,5)+volVars.moleFraction(1,6)<<"\n"
//                           <<std::endl;
//
//                   std::cout<< "precipitatevolumeFractions\n"
//                           << volVars.precipitateVolumeFraction(0) << "primary \n" // precipitateVolumeFraction_[phaseIdx - numPhases], which makes it sphaseIdx
//                           << volVars.precipitateVolumeFraction(1) << "secondary \n"
//                           << volVars.precipitateVolumeFraction(2) << "aceto \n"
//                           << volVars.precipitateVolumeFraction(3) << "hydro \n"
//                           << volVars.precipitateVolumeFraction(4) << "methy \n"
//                           << volVars.precipitateVolumeFraction(5) << "CCoal \n"
//                           << "sum of precipitatevolumeFractions = " <<volVars.precipitateVolumeFraction(0)
//                           +volVars.precipitateVolumeFraction(1)+volVars.precipitateVolumeFraction(2)
//                           +volVars.precipitateVolumeFraction(3)+volVars.precipitateVolumeFraction(4)
//                           +volVars.precipitateVolumeFraction(5)<< "\n"
//                           <<std::endl;
//                   std::cout<< "DebugProb1 source \n" << source <<std::endl;
//                   std::cout<< "DebugProb1 initxnCH4\n" << initxnCH4_ <<std::endl;
//               }

        Valgrind::CheckDefined(source);
    }
    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             int scvIdx) const
    {
        return bothPhases; //wPhaseOnly;
    }

private:

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
     *
     * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
     */
    static Scalar massTomoleFrac_(Scalar XwNaCl)
    {
       const Scalar Mw = 18.015e-3; /* molecular weight of water [kg/mol] */
       const Scalar Ms = 58.44e-3; /* molecular weight of NaCl  [kg/mol] */ //TODO

       const Scalar X_NaCl = XwNaCl;
       /* XlNaCl: conversion from mass fraction to mol fraction */
       auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xwNaCl;
    }

    int nTemperature_;
    int nPressure_;
    int freqMassOutput_;
    PrimaryVariables storageLastTimestep_;
    Scalar lastMassOutputTime_;
    std::string name_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    Scalar Salinity_;
    Scalar reservoirPressure_;
    Scalar innerPressure_;
    Scalar outerPressure_;
    Scalar temperature_;
    Scalar initLiqSaturation_;
    Scalar outerLiqSaturation_;
    Scalar innerLiqSaturation_;

    Scalar initxwAcetate_;
    Scalar initxwAmendment_;
    Scalar initxwRMethyl_;
    Scalar initxnH2_;
    Scalar initxnCH4_;
    Scalar initPhiCCoal_;
    Scalar initPhiPrimBac_;
    Scalar initPhiSecBac_;
    Scalar initPhiAcetoArch_;
    Scalar initPhiMethyArch_;
    Scalar initPhiHydroArch_;

    static constexpr Scalar eps_ = 1e-6;
    Scalar reservoirSaturation_;
    std::ofstream outfile;

};
} //end namespace

#endif
