// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Problem where methane should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediates which serve as substrate for archaea in a coal-bed.
 */
#ifndef DUMUX_MECBM_PROBLEM_HH
#define DUMUX_MECBM_PROBLEM_HH

#include <dumux/material/fluidsystems/brinech4co2.hh> // brinech4mecbm.hh
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/implicit/2pmecbmsimon/model.hh>
#include <dumux/implicit/2pmecbmsimon/fluxvariables.hh>
#include <dumux/implicit/2pmecbmsimon/localresidual.hh>
#include <dumux/implicit/2pmecbmsimon/volumevariables.hh>

#include <dumux/material/chemistry/biogeochemistry/mecbmsimonreactions.hh>   // Microbes and some equations are hear

#include "mecbmsimonspatialparams.hh"
#include "mecbmco2tables.hh"

namespace Dumux
{

template <class TypeTag>
class MECBMProblem;

namespace Properties
{
NEW_TYPE_TAG(MECBMProblem, INHERITS_FROM(BoxModel, TwoPMECBM, MECBMSpatialParams));
//NEW_TYPE_TAG(MECBMProblem, INHERITS_FROM(TwoPMECBM, MECBMSpatialParams));
//NEW_TYPE_TAG(MECBMBoxProblem, INHERITS_FROM(BoxModel, MECBMProblem));
//NEW_TYPE_TAG(MECBMCCProblem, INHERITS_FROM(CCModel, MECBMProblem));

// Set the grid type
SET_TYPE_PROP(MECBMProblem, Grid, Dune::YaspGrid<2>);

// Set the problem property
SET_TYPE_PROP(MECBMProblem, Problem, MECBMProblem<TypeTag>);

////Set the CO2 tables used.
//SET_TYPE_PROP(MECBMProblem, CO2_Tables, Dumux::MECBMProblem::CO2Tables);

// Set fluid configuration and chemistry
SET_PROP(MECBMProblem, Chemistry)
{
    typedef Dumux::MECBMReactions<TypeTag> type;
};
//SET_PROP(MECBMProblem, FluidSystem)
//{
//    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//    typedef Dumux::MECBMProblem::CO2Tables CO2Tables;
//    typedef FluidSystems::BrineCH4<TypeTag, Scalar, CO2Tables, H2O<Scalar>, true/*useComplexrelations=*/> type;
//};
SET_PROP(MECBMProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::MECBM::CO2Tables CO2Tables;
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2OTabulated;
    static const bool useComplexRelations = true;
    typedef Dumux::FluidSystems::BrineCH4CO2<TypeTag, Scalar, CO2Tables, H2OTabulated, useComplexRelations> type;
};

// Set the spatial parameters
SET_TYPE_PROP(MECBMProblem, SpatialParams, MECBMSpatialParams<TypeTag>);
//SET_TYPE_PROP(MECBMProblem, VolumeVariables, TwoPMECBMVolumeVariables<TypeTag>);
//SET_TYPE_PROP(MECBMProblem, LocalResidual, TwoPMECBMLocalResidual<TypeTag>);
SET_TYPE_PROP(MECBMProblem, Model, TwoPMECBMModel<TypeTag>);
// Enable gravity
SET_BOOL_PROP(MECBMProblem, ProblemEnableGravity, false);
//Set properties here to override the default property settings in the model.
//SET_INT_PROP(MECBMProblem, ReplaceCompEqIdx, 1);
//SET_INT_PROP(MECBMProblem, Formulation, TwoPNCFormulation::pgSl);
}

/*!
 * \ingroup TwoPMECBMModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where water with nutrients is injected in a gas reservoir and then
 * microbially enhanced coal bed methane (MECBM) production starts.
 *
 * The domain is sized XYm times YXm and contains no extraction or injection well well.
 * Primary and secondary bacteria grow on coal and produce Hydrogen, Acetate (and Methyl-Groups) = intermediates.
 * Amendment is added to the system, which enhaces the grow of secondary bacteria.
 * By the amendment stimulation they grow more and produce more intermediates.
 * Archaea (three types) then convert the intermediates to Methane and CO2.
 * Ad- and Desorption of Methane and CO2 on coal is considered.
 * CO2 is more likely to sorb on coal, which is why it replaces the already stored CH4.
 * Here, the system is assumed to be isothermal.
 *
 * For the batch experiment Neumann no-flow boundary conditions are applied all around.
 *
 * The model uses mole fractions of dissolved components and volume fractions of biomass and coal as primary variables.
 * Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref MECBMModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_boxmecbmsimon</tt>
 */
template <class TypeTag>
class MECBMProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef MECBMProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {

        pressureIdx     = Indices::pressureIdx,
        switchIdx       = Indices::switchIdx,      //Saturation ?
        numComponents   = FluidSystem::numComponents,

        xwAcetateIdx    = FluidSystem::AcetateIdx,
        xwAmendmentIdx  = FluidSystem::AmendmentIdx,
        xwRMethylIdx    = FluidSystem::RMethylIdx,
        xwH2Idx         = FluidSystem::H2Idx,
        xwCH4Idx        = FluidSystem::CH4Idx,
        xwNaClIdx       = FluidSystem::NaClIdx,
        xwTCIdx         = FluidSystem::TCIdx,

        phiPrimBacIdx   = FluidSystem::numComponents,//FluidSystem::PrimeBacIdx,
        phiSecBacIdx    = FluidSystem::numComponents +1,//FluidSystem::SecBacIdx,
        phiAcetoArchIdx = FluidSystem::numComponents +2,//FluidSystem::AcetoIdx,
        phiHydroArchIdx = FluidSystem::numComponents +3,//FluidSystem::HydroIdx,
        phiMethyArchIdx = FluidSystem::numComponents +4,//FluidSystem::MethyIdx,
        phiCCoalIdx     = FluidSystem::numComponents +5,//FluidSystem::CCoalIdx

        //Indices of the components
        wCompIdx     = FluidSystem::BrineIdx,
        nCompIdx     = FluidSystem::CH4Idx,
        CH4Idx     = FluidSystem::CH4Idx,
        AcetateIdx   = FluidSystem::AcetateIdx,
        AmendmentIdx = FluidSystem::AmendmentIdx,
        RMethylIdx   = FluidSystem::RMethylIdx,
        H2Idx        = FluidSystem::H2Idx,
        TCIdx        = FluidSystem::TCIdx,
//        HCO3Idx      = FluidSystem::HCO3Idx,
//        CO3Idx       = FluidSystem::CO3Idx,
        //Indices of the phases
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
//        sPhaseIdx = FluidSystem::sPhaseIdx,

        //Index of the primary component of G and L phase
//         conti0EqIdx = Indices::conti0EqIdx,
//         contiTotalMassIdx = conti0EqIdx + FluidSystem::CH4Idx,
//         precipBiofilmEqIdx = Indices::conti0EqIdx + FluidSystem::numComponents,
//         contiWEqIdx = conti0EqIdx + FluidSystem::BrineIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };



public:
    MECBMProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        Dune::FMatrixPrecision<>::set_singular_limit(1e-35);

        initxwCH4_              = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwCH4);
        initxwH2_               = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwH2);
        initxwAcetate_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwAcetate);
        initxwAmendment_        = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwAmendment);
        initxwRMethyl_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwRMethyl);
        initxwTC_               = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwTC);
        initPhiCCoal_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiCCoal);
        initPhiPrimBac_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiPrimBac);
        initPhiSecBac_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiSecBac);
        initPhiAcetoArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiAcetoArch);
        initPhiHydroArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiHydroArch);
        initPhiMethyArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiMethyArch);
        initGasSaturation_      = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initGasSaturation);
        Salinity_               = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, Salinity);
        initiallyTwoPhases_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Initial, initiallyTwoPhases);

        reservoirPressure_      = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, ReservoirPressure);
        reservoirSaturation_    = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, reservoirSaturation);
        temperature_            = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Temperature);
        innerPressure_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InnerPressure);
        outerPressure_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OuterPressure);
        outerLiqSaturation_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OuterLiqSaturation);
        innerLiqSaturation_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InnerLiqSaturation);

        VCH4_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SorptionCoefficients, VCH4);
        bCH4_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SorptionCoefficients, bCH4);
        VCO2_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SorptionCoefficients, VCO2);
        bCO2_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SorptionCoefficients, bCO2);

        nTemperature_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FluidSystem, NTemperature);
        nPressure_              = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FluidSystem, NPressure);
        pressureLow_            = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, PressureLow);
        pressureHigh_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, PressureHigh);
        temperatureLow_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, TemperatureLow);
        temperatureHigh_        = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, TemperatureHigh);
        name_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        freqMassOutput_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqMassOutput);
        storageLastTimestep_    = 0.0;
        lastMassOutputTime_     = 0.0;

//        FluidSystem::init();
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);
    }

    ~MECBMProblem()
    {
        outfile.close();
    }

    bool shouldWriteOutput() const
    {
        return this->timeManager().timeStepIndex() % 1 == 0 ||
               this->timeManager().episodeWillBeFinished() ||
               this->timeManager().willBeFinished();
    }

    bool shouldWriteRestartFile() const
    {
        return (this->timeManager().time() > 200000 && this->timeManager().time() < 202000 );
    }


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; }

    /*!
     * \brief Returns the V constant for ad- and desorption for each component in \f$\mathrm{[mol/m^3]}\f$.
     *
     * \param compIdx The component index
     */
    const Scalar V(int compIdx) const
    {
        Scalar V_[numComponents] = {};

        V_[CH4Idx] = VCH4_*1000; // *1000 for conversion from mol/l to mol/m3
        V_[TCIdx]  = VCO2_*1000;
        return V_[compIdx];
    }

    /*!
     * \brief Returns the b constant for ad- and desorption for each component in \f$\mathrm{[1/Pa]}\f$.
     *
     * \param compIdx The component index
     */
    const Scalar b(int compIdx) const
    {
        Scalar b_[numComponents] = {};
        b_[CH4Idx] = bCH4_;
        b_[TCIdx]  = bCO2_;
        return b_[compIdx];
    }

    /*!
     * \name Boundary conditions
     */

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition &globalPos) const
    {
//        const Scalar rMax = this->bBoxMax()[0]; // outerRadius_;
//        const Scalar rMin = this->bBoxMin()[0];

        // default to Neumann
        bcTypes.setAllNeumann();

        // Constant pressure  at reservoir boundary (Dirichlet condition)
//        if(globalPos[0] > rMax - eps_)
//            bcTypes.setAllDirichlet();

        // Constant pressure at well (Dirichlet condition)
//        if(globalPos[0] < rMin + eps_)
//            bcTypes.setAllDirichlet();

    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
/*    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        const Scalar rMax = this->bBoxMax()[0];
        const Scalar rMin = this->bBoxMin()[0];

        if(globalPos[0] > rMax - eps_)
        {
            values[pressureIdx]   = outerPressure_ ; // Outer boundary pressure bar
            values[switchIdx]     = outerLiqSaturation_; // Saturation outer boundary
            values[phiBiofilmIdx]     = massTomoleFrac_(Salinity_);// mole fraction salt
            values[precipBiofilmIdx] = 0.0;// precipitated salt
        }

        if(globalPos[0] < rMin + eps_)
        {

            values[pressureIdx]      = innerPressure_ ;         // Inner boundary pressure bar
            values[switchIdx]        = innerLiqSaturation_;     // Saturation inner boundary
            values[phiAcetateIdx]     = 0;           // Initial concentration of Acetate
            values[phiAmendmentIdx]   = 0;         // Initial concentration of Amendment
            values[phiRMethylIdx]      = 0;            // Initial concentration of Methyl
            values[PhiH2Idx]          = 0;                // Initial concentration of Hydrogen
            values[precipBiofilmIdx] = 0.0;                     // precipitated salt?

        }
    }*/

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 int scvIdx,
                 int boundaryFaceIdx) const
    {
        values = 0.0;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume due to chemical reactions.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */

      void source(PrimaryVariables &q,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                  int scvIdx) const
      {
          q = 0;
      }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values[pressureIdx]     = reservoirPressure_;
        if(initiallyTwoPhases_)
            values[switchIdx]   = initGasSaturation_;         // Sl primary variable
        else
            values[xwCH4Idx]    = initxwCH4_;                 // Initial mole fraction of Methane/CH4

        values[xwH2Idx]         = initxwH2_;                  // Initial mole fraction of Hydrogen

        values[xwAmendmentIdx]  = initxwAmendment_;           // Initial mole fraction of Amendment
        values[xwAcetateIdx]    = initxwAcetate_;             // Initial mole fraction of Acetate
        values[xwRMethylIdx]    = initxwRMethyl_;             // Initial mole fraction of Methyl
        values[xwTCIdx]         = initxwTC_;                  // Initial mole fraction of total inorganic carbon
        values[phiCCoalIdx]     = initPhiCCoal_;              // Initial volume fraction
        values[phiPrimBacIdx]   = initPhiPrimBac_;            // Initial volume fraction
        values[phiSecBacIdx]    = initPhiSecBac_;             // Initial volume fraction
        values[phiAcetoArchIdx] = initPhiAcetoArch_;          // Initial volume fraction
        values[phiMethyArchIdx] = initPhiMethyArch_;          // Initial volume fraction
        values[phiHydroArchIdx] = initPhiHydroArch_ ;         // Initial volume fraction
        values[xwNaClIdx]       = massTomoleFrac_(Salinity_); // mole fraction
    }

    /*!
     * \name Volume terms
     */

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void solDependentSource(PrimaryVariables &source,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            int scvIdx,
                            const ElementVolumeVariables &elemVolVars) const
    {
        source = 0;
        const auto &volVars = elemVolVars[scvIdx];

        Scalar dt = this->timeManager().timeStepSize();

        Chemistry chemistry;
        chemistry.reactionSource(source,
                volVars,
                dt);

        Valgrind::CheckDefined(source);
    }
    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             int scvIdx) const
    {
        if(initiallyTwoPhases_)
            return bothPhases;
        else
            return wPhaseOnly;//
    }

    void postTimeStep()
    {
        // Calculate masses and flux over a layer
        //PrimaryVariables mass;
//        PrimaryVariables massGas(0.0);
//        PrimaryVariables massH2O(0.0);
//
//                this->model().globalPhaseStorage(massH2O, wPhaseIdx);
//                this->model().globalPhaseStorage(massGas, nPhaseIdx);
//
//        ////        computeStorage(PrimaryVariables &storage, int scvIdx, bool usePrevSol)
//                std::cout<< " MassH2O_l:   "; std::cout.width(11); std::cout<< massH2O[wCompIdx] <<" kg, "
//                << " MassCH4_l:   "; std::cout.width(11); std::cout<< massH2O[nCompIdx] <<" kg, "
//                <<std::endl;
//                std::cout<< " MassH2O_g:   "; std::cout.width(11); std::cout << massGas[wCompIdx] << " kg, "
//                << " MassCH4_g:   "; std::cout.width(11); std::cout << massGas[nCompIdx] << " kg, "
//                <<std::endl;
    }
private:

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
     *
     * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
     */
    static Scalar massTomoleFrac_(Scalar XwNaCl)
    {
       const Scalar Mw = 18.015e-3; /* molecular weight of water [kg/mol] */
       const Scalar Ms = 58.44e-3; /* molecular weight of NaCl  [kg/mol] */ //TODO

       const Scalar X_NaCl = XwNaCl;
       /* XlNaCl: conversion from mass fraction to mol fraction */
       auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xwNaCl;
    }

    int nTemperature_;
    int nPressure_;
    int freqMassOutput_;
    PrimaryVariables storageLastTimestep_;
    Scalar lastMassOutputTime_;
    std::string name_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    Scalar Salinity_;
    Scalar reservoirPressure_;
    Scalar innerPressure_;
    Scalar outerPressure_;
    Scalar temperature_;
    Scalar initGasSaturation_;
    Scalar outerLiqSaturation_;
    Scalar innerLiqSaturation_;

    Scalar VCH4_;
    Scalar bCH4_;
    Scalar VCO2_;
    Scalar bCO2_;
    Scalar globalPhaseStorage_;

    Scalar initxwAcetate_;
    Scalar initxwAmendment_;
    Scalar initxwRMethyl_;
    Scalar initxwTC_;
    Scalar initxwH2_;
    Scalar initxwCH4_;
    Scalar initPhiCCoal_;
    Scalar initPhiPrimBac_;
    Scalar initPhiSecBac_;
    Scalar initPhiAcetoArch_;
    Scalar initPhiMethyArch_;
    Scalar initPhiHydroArch_;

    static constexpr Scalar eps_ = 1e-6;
    Scalar reservoirSaturation_;
    std::ofstream outfile;
    bool initiallyTwoPhases_=false;

};
} //end namespace

#endif
