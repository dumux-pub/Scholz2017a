// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Problem where methane should be producded by adding microbes and nutrients to a coal reservoir.
 */
#ifndef DUMUX_MECBM_PROBLEM_HH
#define DUMUX_MECBM_PROBLEM_HH

#include <dumux/material/fluidsystems/brinech4co2.hh> // brinech4co2.hh
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/implicit/2pmecbmsimon/model.hh>
#include <dumux/implicit/2pmecbmsimon/fluxvariables.hh>
#include <dumux/implicit/2pmecbmsimon/localresidual.hh>
#include <dumux/implicit/2pmecbmsimon/volumevariables.hh>

#include <dumux/material/chemistry/biogeochemistry/mecbmsimonreactions.hh>   // Microbes and some equations are hear

#include "mecbmsimonspatialparams.hh"
#include "mecbmco2tables.hh"

namespace Dumux
{

template <class TypeTag>
class Adsorption1pProblem;

namespace Properties
{
NEW_TYPE_TAG(Adsorption1pProblem, INHERITS_FROM(BoxModel, TwoPMECBM, MECBMSpatialParams));
//NEW_TYPE_TAG(MECBMProblem, INHERITS_FROM(TwoPMECBM, MECBMSpatialParams));
//NEW_TYPE_TAG(MECBMBoxProblem, INHERITS_FROM(BoxModel, MECBMProblem));
//NEW_TYPE_TAG(MECBMCCProblem, INHERITS_FROM(CCModel, MECBMProblem));

// Set the grid type
SET_TYPE_PROP(MECBMProblem, Grid, Dune::YaspGrid<2>);

// Set the problem property
SET_TYPE_PROP(MECBMProblem, Problem, MECBMProblem<TypeTag>);

////Set the CO2 tables used.
//SET_TYPE_PROP(MECBMProblem, CO2_Tables, Dumux::MECBMProblem::CO2Tables);

// Set fluid configuration and chemistry
SET_PROP(MECBMProblem, Chemistry)
{
    typedef Dumux::MECBMReactions<TypeTag> type;
};
//SET_PROP(MECBMProblem, FluidSystem)
//{
//    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//    typedef Dumux::MECBMProblem::CO2Tables CO2Tables;
//    typedef FluidSystems::BrineCH4<TypeTag, Scalar, CO2Tables, H2O<Scalar>, true/*useComplexrelations=*/> type;
//};
SET_PROP(MECBMProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::MECBM::CO2Tables CO2Tables;
    typedef Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>> H2OTabulated;
    static const bool useComplexRelations = true;
    typedef Dumux::FluidSystems::BrineCH4CO2<TypeTag, Scalar, CO2Tables, H2OTabulated, useComplexRelations> type;
};

// Set the spatial parameters
SET_TYPE_PROP(MECBMProblem, SpatialParams, MECBMSpatialParams<TypeTag>);
//SET_TYPE_PROP(MECBMProblem, VolumeVariables, TwoPMECBMVolumeVariables<TypeTag>);
//SET_TYPE_PROP(MECBMProblem, LocalResidual, TwoPMECBMLocalResidual<TypeTag>);
//SET_TYPE_PROP(MECBMProblem, Model, TwoPMECBMModel<TypeTag>);
// Enable gravity
SET_BOOL_PROP(MECBMProblem, ProblemEnableGravity, false);
//Set properties here to override the default property settings in the model.
//SET_INT_PROP(MECBMProblem, ReplaceCompEqIdx, 1);
SET_INT_PROP(MECBMProblem, Formulation, TwoPNCFormulation::pnsw);
}

/*!
 * \ingroup TwoPMECBMModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where water with nutrients is injected in a gas reservoir and then microbially enhanced coal bed methane production starts.
 *
 * The domain is sized XYm times YXm and contains an extraction well.
 * TODO: Adapt more!
 * To flush this precipitated salt, water is injected through the gas extraction well in order to dissolve the precipitated salt increasing the permeability and thereby achieving high gas extraction rates later. Here, the system is assumed to be isothermal.
 * Neumann no-flow boundary condition is applied at the top and bottom boundary and Dirichlet boundary condition is used on the right and left sides.
 * The injected water phase migrates downwards due to increase in density as the precipitated salt dissolves.
 *
 * The model uses mole fractions of dissolved components and volume fractions of precipitated salt as primary variables. Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref TwoPNCMinModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_boxmecbmsimon</tt>
 */
template <class TypeTag>
class MECBMProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef MECBMProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        pressureIdx     = Indices::pressureIdx,
        switchIdx       = Indices::switchIdx,      //Saturation ?
        numSPhases = FluidSystem::numSPhases,

        xwAcetateIdx    = FluidSystem::AcetateIdx,
        xwAmendmentIdx  = FluidSystem::AmendmentIdx,
        xwRMethylIdx    = FluidSystem::RMethylIdx,
        xwH2Idx         = FluidSystem::H2Idx,
        xwCH4Idx        = FluidSystem::CH4Idx,
        xwNaClIdx       = FluidSystem::NaClIdx,
        xwTCIdx         = FluidSystem::TCIdx,

        phiPrimBacIdx   = FluidSystem::numComponents,//FluidSystem::PrimeBacIdx,
        phiSecBacIdx    = FluidSystem::numComponents +1,//FluidSystem::SecBacIdx,
        phiAcetoArchIdx = FluidSystem::numComponents +2,//FluidSystem::AcetoIdx,
        phiHydroArchIdx = FluidSystem::numComponents +3,//FluidSystem::HydroIdx,
        phiMethyArchIdx = FluidSystem::numComponents +4,//FluidSystem::MethyIdx,
        phiCCoalIdx     = FluidSystem::numComponents +5,//FluidSystem::CCoalIdx

        //Indices of the components
        wCompIdx     = FluidSystem::BrineIdx,
        nCompIdx     = FluidSystem::CH4Idx,
        AcetateIdx   = FluidSystem::AcetateIdx,
        AmendmentIdx = FluidSystem::AmendmentIdx,
        RMethylIdx   = FluidSystem::RMethylIdx,
        H2Idx        = FluidSystem::H2Idx,
        TCIdx        = FluidSystem::TCIdx,
//        HCO3Idx      = FluidSystem::HCO3Idx,
//        CO3Idx       = FluidSystem::CO3Idx,
        //Indices of the phases
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
//        sPhaseIdx = FluidSystem::sPhaseIdx,

        //Index of the primary component of G and L phase
         conti0EqIdx = Indices::conti0EqIdx,
//         contiTotalMassIdx = conti0EqIdx + FluidSystem::CH4Idx,
//         precipBiofilmEqIdx = Indices::conti0EqIdx + FluidSystem::numComponents,
//         contiWEqIdx = conti0EqIdx + FluidSystem::BrineIdx,

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    MECBMProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        Dune::FMatrixPrecision<>::set_singular_limit(1e-35);

        initxwCH4_              = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwCH4);
        initxwH2_               = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwH2);
        initxwAcetate_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwAcetate);
        initxwAmendment_        = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwAmendment);
        initxwRMethyl_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwRMethyl);
        initxwTC_               = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initxwTC);
        initPhiCCoal_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiCCoal);
        initPhiPrimBac_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiPrimBac);
        initPhiSecBac_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiSecBac);
        initPhiAcetoArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiAcetoArch);
        initPhiHydroArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiHydroArch);
        initPhiMethyArch_       = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPhiMethyArch);
        initGasSaturation_      = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initGasSaturation);
        Salinity_               = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, Salinity);
        initiallyTwoPhases_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Initial, initiallyTwoPhases);

        reservoirPressure_      = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, ReservoirPressure);
        reservoirSaturation_    = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, reservoirSaturation);
        temperature_            = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Temperature);
        innerPressure_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InnerPressure);
        outerPressure_          = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OuterPressure);
        outerLiqSaturation_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, OuterLiqSaturation);
        innerLiqSaturation_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InnerLiqSaturation);

        VCH4_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SorptionCoefficients, VCH4);
        bCH4_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SorptionCoefficients, bCH4);
        VCO2_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SorptionCoefficients, VCO2);
        bCO2_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SorptionCoefficients, bCO2);

        injQ_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injVolumeflux);
        injTC_                  = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injTC);
        injCH4_                 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, injCH4);

        nTemperature_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FluidSystem, NTemperature);
        nPressure_              = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FluidSystem, NPressure);
        pressureLow_            = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, PressureLow);
        pressureHigh_           = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, PressureHigh);
        temperatureLow_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, TemperatureLow);
        temperatureHigh_        = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, TemperatureHigh);
        name_                   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        freqMassOutput_         = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqMassOutput);
        storageLastTimestep_    = 0.0;
        lastMassOutputTime_     = 0.0;

//        FluidSystem::init();
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);

        this->timeManager().startNextEpisode(endEpisode(0));

    }

    ~MECBMProblem()
    {
        outfile.close();
    }

    bool shouldWriteOutput() const
    {
        return this->timeManager().timeStepIndex() % 1 == 0 ||
               this->timeManager().episodeWillBeOver() ||
               this->timeManager().willBeFinished();
    }

    bool shouldWriteRestartFile() const
    {
       return false;
//        return (this->timeManager().time() > 200000 && this->timeManager().time() < 202000 );
    }


    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; }

    /*!
     * \brief Returns the V constant for sorption of Methane CH4.
     */
    Scalar VCH4() const
    { return VCH4_; }

    /*!
     * \brief Returns the b constant for sorption of Methane CH4.

     */
    Scalar bCH4() const
    { return bCH4_; }

    /*!
     * \brief Returns the V constant for sorption of CO2.
     */
    Scalar VCO2() const
    { return VCO2_; }

    /*!
     * \brief Returns the b constant for sorption of CO2.

     */
    Scalar bCO2() const
    { return bCO2_; }

    /*!
     * \name Boundary conditions
     */

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    void boundaryTypesAtPos(BoundaryTypes &bcTypes, const GlobalPosition &globalPos) const
    {
        const Scalar zMax = this->bBoxMax()[dim-1]; // outerRadius_;
        const Scalar zMin = this->bBoxMin()[dim-1];

        // default to Neumann
        bcTypes.setAllNeumann();

        // Constant pressure  at reservoir boundary (Dirichlet condition)
        if(globalPos[1] > zMax - eps_)
            {
//            bcTypes.setDirichlet(pressureIdx);
            bcTypes.setOutflow(pressureIdx);
            for (int i=switchIdx; i< numEq - numSPhases; ++i)
            {
              bcTypes.setOutflow(i);
//              bcTypes.setDirichlet(i);
            }
            }

//
//            if(globalPos[1] > zMax - eps_)
//            bcTypes.setOutflow());
//        // Constant pressure at well (Dirichlet condition)
//        if(globalPos[0] < rMin + eps_)
//            bcTypes.setAllDirichlet();

    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        const Scalar zMax = this->bBoxMax()[dim-1];
        const Scalar zMin = this->bBoxMin()[dim-1];

        if(globalPos[1] > zMax - eps_)
        {
            values[pressureIdx]     = reservoirPressure_;
        }

//        if(globalPos[0] < zMin + eps_)
//        {
//
//            values[pressureIdx]      = innerPressure_ ;         // Inner boundary pressure bar
//            values[switchIdx]        = innerLiqSaturation_;     // Saturation inner boundary
//            values[phiAcetateIdx]     = 0;           // Initial concentration of Acetate
//            values[phiAmendmentIdx]   = 0;         // Initial concentration of Amendment
//            values[phiRMethylIdx]      = 0;            // Initial concentration of Methyl
//            values[PhiH2Idx]          = 0;                // Initial concentration of Hydrogen
//            values[precipBiofilmIdx] = 0.0;                     // precipitated salt?
//
//        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 int scvIdx,
                 int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos =fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
        Scalar diameter = this->bBoxMax()[0];
        Scalar area = 3.14*diameter*diameter/4;

        int episodeIdx = this->timeManager().episodeIndex()-1;
        int InjProcess = injection(episodeIdx);
        Scalar waterFlux = injQ_ / area; //[m/s] v=1 m/d = 1.1547e-5 m/s

        if(globalPos[1] <= eps_) // standard: no injection
        {
            values = 0.0;  //

        if (InjProcess == -99) // no injection
        {
            values = 0.0; //mol/m²/s
        }
        else if (InjProcess == 1)       //methane injection
        {
//            values[conti0EqIdx + wCompIdx] = - waterFlux * 996 / FluidSystem::molarMass(wCompIdx);
            values[conti0EqIdx + nCompIdx] = - waterFlux * injCH4_ / FluidSystem::molarMass(nCompIdx);
        }
        else if (InjProcess == 2)       //CO2 injection
        {
//            values[conti0EqIdx + wCompIdx] = - waterFlux * 996 / FluidSystem::molarMass(wCompIdx);
            values[conti0EqIdx + TCIdx]    = - waterFlux * injTC_ / FluidSystem::molarMass(TCIdx);
        }
        else if (InjProcess == 0)
        {
//             values[conti0EqIdx + wCompIdx] = -waterFlux * 996 / FluidSystem::molarMass(wCompIdx);
        }


        }
        else
            values = 0.0; // kg/m/s
    }
//
//    void solDependentNeumann(PrimaryVariables &values,
//            const Element &element,
//            const FVElementGeometry &fvGeometry,
//            const Intersection &is,
//            const int scvIdx,
//            const int boundaryFaceIdx,
//            const ElementVolumeVariables &elemVolVars) const
//    {
//        values = 0;
//        const GlobalPosition globalPos = fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
//
//        //Top boundary condition: Recharge
////        if(boundaryId == topBoundary)
////        {
////            values[pressureIdx] = -gWRegenerationRate_; //[kg/sq/s]
////        }
//
//        //Set no flow boundary conditions for the symmetry axes of the generic models
//        Scalar diameter = this->bBoxMax()[0];
//        Scalar waterFlux = injQ_/(3.14*diameter*diameter/4.); //[m/s] v=1 m/d = 1.1547e-5 m/s
//
//        if(globalPos[1] <= eps_)
//        {
//            values[conti0EqIdx + wCompIdx] = waterFlux*996/FluidSystem::molarMass(wCompIdx);
//        }
//        else
//            values = 0.0;
//    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume due to chemical reactions.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
//TODO: source needed for compiling, but why? Check later.
      void source(PrimaryVariables &q,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                  int scvIdx) const
      {
          q = 0;
      }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        const Scalar zMin = this->bBoxMin()[dim-1];
        Scalar diameter = this->bBoxMax()[0];
        values[pressureIdx]     = innerPressure_;
        if(initiallyTwoPhases_)
            values[switchIdx]   = initGasSaturation_;         // Sl primary variable
        else
            values[xwCH4Idx]    = initxwCH4_;                 // Initial mole fraction of Methane/CH4

        values[xwH2Idx]         = initxwH2_;                  // Initial mole fraction of Hydrogen


        values[xwAcetateIdx]    = initxwAcetate_;             // Initial mole fraction of Acetate
        values[xwRMethylIdx]    = initxwRMethyl_;             // Initial mole fraction of Methyl
        values[phiCCoalIdx]     = initPhiCCoal_;              // Initial volume fraction
        values[phiPrimBacIdx]   = initPhiPrimBac_;            // Initial volume fraction
        values[phiSecBacIdx]    = initPhiSecBac_;             // Initial volume fraction
        values[phiAcetoArchIdx] = initPhiAcetoArch_;          // Initial volume fraction
        values[phiMethyArchIdx] = initPhiMethyArch_;          // Initial volume fraction
        values[phiHydroArchIdx] = initPhiHydroArch_ ;         // Initial volume fraction
        values[xwNaClIdx]       = massTomoleFrac_(Salinity_); // mole fraction
//        if (globalPos[1] < zMin + eps_)
            values[xwTCIdx]         = initxwTC_;                  // Initial mole fraction of total inorganic carbon


       /* if (globalPos[0] > 2.0)
            values[precipBiofilmIdx] = initPrecipitatedSalt1_; // [kg/m^3]
            values[phiCCoalIdx]  = massTomoleFrac_(Salinity_);     // mole fraction
            values[phiAcetateIdx]  = initCAcetate_;             // Initial concentration of Acetate;
            values[phiAmendmentIdx]= initCAmendment_;           // Initial concentration of Amendment;
            values[phiMethylIdx]   = initCMethyl_;              // Initial concentration of Methyl;
            values[phiH2Idx]       = initCH2_;                  // Initial concentration of Hydrogen; */
           /* else
            values[precipBiofilmIdx] = initPrecipitatedSalt1_; // [kg/m^3]*/
    }

    /*!
     * \name Volume terms
     */

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void solDependentSource(PrimaryVariables &source,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            int scvIdx,
                            const ElementVolumeVariables &elemVolVars) const
    {
        source = 0;
//        const auto &volVars = elemVolVars[scvIdx];

        Scalar dt = this->timeManager().timeStepSize();

//        Chemistry chemistry;
//        chemistry.reactionSource(source,
//                volVars,
//                dt);

//        std::cout << source << "DebugProb2"<< std::endl;

//        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
//               if(globalPos[0]<eps_)
//               {
//                   std::cout<< "moleFractions\n"
//                           << volVars.moleFraction(0,0) << "Brine \n" //moleFraction(phaseIdx,compIdx)
//                           << volVars.moleFraction(0,1) << "Gas CH4 \n"
//                           << volVars.moleFraction(0,2) << "Acetate \n"
//                           << volVars.moleFraction(0,3) << "Amendment \n"
//                           << volVars.moleFraction(0,4) << "RMethyl \n"
//                           << volVars.moleFraction(0,5) << "H2 \n"
//                           << volVars.moleFraction(0,6) << "NaCl \n"
//
//                           << "sum of liquid moleFractions = " <<volVars.moleFraction(0,0)
//                           +volVars.moleFraction(0,1)+volVars.moleFraction(0,2)
//                           +volVars.moleFraction(0,3)+volVars.moleFraction(0,4)
//                           +volVars.moleFraction(0,5)+volVars.moleFraction(0,6)<<"\n"
//                           << volVars.moleFraction(1,0) << "Gas Phase Brine \n"
//                           << volVars.moleFraction(1,1) << "Gas Phase Gas CH4 \n"
//                           << volVars.moleFraction(1,2) << "Gas Phase Acetate \n"
//                           << volVars.moleFraction(1,3) << "Gas Phase Amendment \n"
//                           << volVars.moleFraction(1,4) << "Gas Phase RMethyl \n"
//                           << volVars.moleFraction(1,5) << "Gas Phase H2 \n"
//                           << volVars.moleFraction(1,6) << "Gas Phase NaCl \n"
//                           << "sum of gas moleFractions = " <<volVars.moleFraction(1,0)
//                           +volVars.moleFraction(1,1)+volVars.moleFraction(1,2)
//                           +volVars.moleFraction(1,3)+volVars.moleFraction(1,4)
//                           +volVars.moleFraction(1,5)+volVars.moleFraction(1,6)<<"\n"
//                           <<std::endl;
//
//                   std::cout<< "precipitatevolumeFractions\n"
//                           << volVars.precipitateVolumeFraction(0) << "primary \n" // precipitateVolumeFraction_[phaseIdx - numPhases], which makes it sphaseIdx
//                           << volVars.precipitateVolumeFraction(1) << "secondary \n"
//                           << volVars.precipitateVolumeFraction(2) << "aceto \n"
//                           << volVars.precipitateVolumeFraction(3) << "hydro \n"
//                           << volVars.precipitateVolumeFraction(4) << "methy \n"
//                           << volVars.precipitateVolumeFraction(5) << "CCoal \n"
//                           << "sum of precipitatevolumeFractions = " <<volVars.precipitateVolumeFraction(0)
//                           +volVars.precipitateVolumeFraction(1)+volVars.precipitateVolumeFraction(2)
//                           +volVars.precipitateVolumeFraction(3)+volVars.precipitateVolumeFraction(4)
//                           +volVars.precipitateVolumeFraction(5)<< "\n"
//                           <<std::endl;
//                   std::cout<< "DebugProb1 source \n" << source <<std::endl;
//                   std::cout<< "DebugProb1 initxwCH4\n" << initxwCH4_ <<std::endl;
//               }

        Valgrind::CheckDefined(source);
    }
    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Element& element,
                             const FVElementGeometry& fvGeometry,
                             int scvIdx) const
    {
        if(initiallyTwoPhases_)
            return bothPhases;
        else
            return wPhaseOnly;//
    }

    /*!
    * \brief  Called as the last function after a successful time integration.
    * Here, the current time, episode, and the injection type during the current episode
    * are printed as information to the user.
    */
    void postTimeStep()
    {
        double time = this->timeManager().time();
        double dt = this->timeManager().timeStepSize();

        int episodeIdx = this->timeManager().episodeIndex()-1;
        int InjProcess = injection(episodeIdx);

        if (this->gridView().comm().rank() == 0)
        {
           std::cout<< " Time: "<<time+dt<< ", Episode: "<<episodeIdx<<", injection process: "<<InjProcess<<", injection of "<<injQ_/(60*60*24)*1e3<<" [l/day]"<<std::endl;
           std::cout<<"==============================================******==================================================="<<std::endl;
       }
    }

    /*!
    * \brief Manage the episodes.
    * The duration of the next episode set using the private function Scalar episodeEnd(episodeIdx).
    * Also, the time-step size is set to 1 as a new episode usually means a changed injection type
    * which im many cases causes difficulties for the Newton convergence.
    */
void episodeEnd()
{
    int episodeIdx = this->timeManager().episodeIndex()-1;
        Scalar tEpisode= endEpisode(episodeIdx + 1)-endEpisode(episodeIdx);

        this->timeManager().startNextEpisode(tEpisode);
   //     this->timeManager().setTimeStepSize(tEpisode / 100);
   //        if (Injection(episodeIdx + 1) == -1)
   //              this->timeManager().setTimeStepSize(0.01);
   //        else if (Injection(episodeIdx + 1) == 0 || Injection(episodeIdx + 1) == 3)
                   this->timeManager().setTimeStepSize(0.1);
   //        else
   //              this->timeManager().setTimeStepSize(10);

           std::cout<< "\n  episode number  " << episodeIdx << " done, starting next episode number  " <<episodeIdx + 1 << ", Injection scheme is : "<<  injection(episodeIdx + 1) << "\n"  <<std::endl;
    }

private:

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
     *
     * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
     */
    static Scalar massTomoleFrac_(Scalar XwNaCl)
    {
       const Scalar Mw = 18.015e-3; /* molecular weight of water [kg/mol] */
       const Scalar Ms = 58.44e-3; /* molecular weight of NaCl  [kg/mol] */ //TODO

       const Scalar X_NaCl = XwNaCl;
       /* XlNaCl: conversion from mass fraction to mol fraction */
       auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xwNaCl;
    }

    static const int injection (int episodeIdx)
    {
        Scalar Inj[4] = {1, 1, 2, 2};
        return Inj[episodeIdx];
    }

        //Returns the episode end times as specified in the injection data file
    static const Scalar endEpisode (int episodeIdx)
    {
        //End times of the episodes in days.
         Scalar endEpi[4] = {1,19, 40,100};

    return 60 * 60 * 24 * endEpi[episodeIdx]; //return times in s
    }

    int nTemperature_;
    int nPressure_;
    int freqMassOutput_;
    PrimaryVariables storageLastTimestep_;
    Scalar lastMassOutputTime_;
    std::string name_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    Scalar Salinity_;
    Scalar reservoirPressure_;
    Scalar innerPressure_;
    Scalar outerPressure_;
    Scalar temperature_;
    Scalar initGasSaturation_;
    Scalar outerLiqSaturation_;
    Scalar innerLiqSaturation_;

    Scalar VCH4_;
    Scalar bCH4_;
    Scalar VCO2_;
    Scalar bCO2_;

    Scalar initxwAcetate_;
    Scalar initxwAmendment_;
    Scalar initxwRMethyl_;
    Scalar initxwTC_;
    Scalar initxwH2_;
    Scalar initxwCH4_;
    Scalar initPhiCCoal_;
    Scalar initPhiPrimBac_;
    Scalar initPhiSecBac_;
    Scalar initPhiAcetoArch_;
    Scalar initPhiMethyArch_;
    Scalar initPhiHydroArch_;

    Scalar injQ_;
    Scalar injTC_;
    Scalar injCH4_;

    static constexpr Scalar eps_ = 1e-6;
    Scalar reservoirSaturation_;
    std::ofstream outfile;
    bool initiallyTwoPhases_=false;

};
} //end namespace

#endif
