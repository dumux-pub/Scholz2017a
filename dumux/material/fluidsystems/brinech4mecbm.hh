// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief @copybrief Dumux::FluidSystems::BrineCH4
 */
#ifndef DUMUX_BRINE_CH4_MECBM_SYSTEM_HH
#define DUMUX_BRINE_CH4_MECBM_SYSTEM_HH

#include <cassert>
#include <dumux/material/idealgas.hh>

#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/components/brine.hh>
#include <dumux/material/components/ch4.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/nacl.hh>
#include <dumux/material/components/biofilm.hh>
#include <dumux/material/components/ccoal.hh>
#include <dumux/material/components/acetate.hh>
#include <dumux/material/components/caram.hh>
#include <dumux/material/components/rmethyl.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/co3.hh>
#include <dumux/material/components/hco3.hh>
#include <dumux/material/components/co2.hh>

#include <dumux/material/binarycoefficients/brine_ch4.hh>
#include <dumux/material/binarycoefficients/brine_co2.hh>
#include <dumux/material/binarycoefficients/h2o_ch4.hh>
#include <dumux/material/binarycoefficients/h2o_h2.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/common/valgrind.hh>
#include <dumux/common/exceptions.hh>

#ifdef DUMUX_PROPERTIES_HH
#include <dumux/common/propertysystem.hh>
#include <dumux/common/basicproperties.hh>
#endif

namespace Dumux
{
namespace FluidSystems
{
/*!
 * \ingroup Fluidsystems
 *
 * \brief A compositional two-phase fluid system with a liquid and a gaseous phase
 *        and \f$H_2O\f$, \f$CH4\f$ and \f$S\f$ (dissolved biofilm nutrients) as components.
 *
 *  This fluidsystem is applied by default with the tabulated version of
 *  water of the IAPWS-formulation.
 *
 *  To change the component formulation (i.e. to use nontabulated or
 *  incompressible water), or to switch on verbosity of tabulation,
 *  specify the water formulation via template arguments or via the property
 *  system, as described in the TypeTag Adapter at the end of the file.
 *
 * \code{.cpp}
 * // Select fluid system
 * SET_PROP(TheSpecificProblemTypeTag, FluidSystem)
 * {
 *     // e.g. to use a simple version of H2O
 *     typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
 *     typedef FluidSystems::H2OCH4<Scalar, SimpleH2O<Scalar> > type;
 * };
 * \endcode
 *
 *   Also remember to initialize tabulated components (FluidSystem::init()), while this
 *   is not necessary for non-tabularized ones.
 *
 * This FluidSystem can be used without the PropertySystem that is applied in Dumux,
 * as all Parameters are defined via template parameters. Hence it is in an
 * additional namespace FluidSystem::.
 * An adapter class using FluidSystem<TypeTag> is also provided
 * at the end of this file.
 */
template <class TypeTag,
          class Scalar,
          class H2Otype = TabulatedComponent<Scalar, H2O<Scalar>>,
          bool useComplexRelations=true>
class BrineCH4
: public BaseFluidSystem<Scalar, BrineCH4<TypeTag, Scalar, H2Otype, useComplexRelations>>
{
    typedef BrineCH4<TypeTag, Scalar, H2Otype, useComplexRelations> ThisType;
    typedef BaseFluidSystem <Scalar, ThisType> Base;

    typedef Dumux::IdealGas<Scalar> IdealGas;

public:

    typedef H2Otype H2O;
    typedef BinaryCoeff::H2O_CH4 H2O_CH4;
    typedef Dumux::CH4<Scalar> CH4;
    typedef BinaryCoeff::Brine_CH4<Scalar> Brine_CH4;
    typedef BinaryCoeff::H2O_H2 H2O_H2;
    typedef Dumux::Brine<Scalar, H2Otype> Brine;
    typedef Dumux::CCoal<Scalar> CCoal;
    typedef Dumux::Acetate<Scalar> Acetate;
    typedef Dumux::CarAm<Scalar> Amendment;
    typedef Dumux::RMethyl<Scalar> RMethyl;
    typedef Dumux::H2<Scalar> H2;
    typedef Dumux::NaCl<Scalar> NaCl;
//    typedef Dumux::CO2<Scalar, CO2Tables> CO2;
    typedef Dumux::CO3<Scalar> CO3;
    typedef Dumux::HCO3<Scalar> HCO3;
    typedef Dumux::Biofilm<TypeTag, Scalar> Biofilm;

    // the type of parameter cache objects. this fluid system does not
    typedef NullParameterCache ParameterCache;

    /****************************************
     * Fluid phase related static parameters
     ****************************************/
    static const int numSecComponents = 0; //needed to set property not used for 2pncMin model
    static const int numPhases  = 2; // liquid and gas phases
    static const int numSPhases = 6;// precipitated solid phases
//    static const int lPhaseIdx  = 0; // index of the liquid phase
//    static const int gPhaseIdx  = 1; // index of the gas phase
//    static const int sPhaseIdx  = 2; // index of the solid phase
    static const int wPhaseIdx  = 0; // index of the wetting phase
    static const int nPhaseIdx  = 1; // index of the non-wetting phase
    static const int PrimBacPhaseIdx   = numPhases; // index of the primary bacteria Phase
    static const int SecBacPhaseIdx    = numPhases + 1; // index of the secondary bacteria Phase
    static const int AcetoArchPhaseIdx = numPhases + 2; // index of the acetoclastic archaea Phase
    static const int HydroArchPhaseIdx = numPhases + 3; // index of the hydrogenotrophic archaea
    static const int MethyArchPhaseIdx = numPhases + 4; // index of the methylotrophic archaea
    static const int CCoalPhaseIdx     = numPhases + 5;

    // export component indices to indicate the main component
    // of the corresponding phase at atmospheric pressure 1 bar
    // and room temperature 20°C:
    static const int wCompIdx = wPhaseIdx;
    static const int nCompIdx = nPhaseIdx;

    /*!
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx index of the phase
     */
    static const char *phaseName(int phaseIdx)
    {
        switch (phaseIdx) {
        case wPhaseIdx: return "liquid";
        case nPhaseIdx: return "gas";
        case PrimBacPhaseIdx: return "Primary Bacteria Phase";
        case SecBacPhaseIdx: return "Secondary Bacteria Phase" ;
        case AcetoArchPhaseIdx: return "Aceto Archaea Phase" ;
        case HydroArchPhaseIdx: return "Hydro Archaea Phase" ;
        case MethyArchPhaseIdx: return "Methyl Archaea Phase" ;
        case CCoalPhaseIdx: return "C Coal Phase" ;
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Return whether a phase is liquid
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isLiquid(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        return phaseIdx != nPhaseIdx;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are independent on the fluid composition. This assumption is true
     * if Henry's law and Rault's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealMixture(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // we assume Henry's and Rault's laws for the water phase and
        // and no interaction between gas molecules of different
        // components, so all phases are ideal mixtures!

        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isCompressible(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // ideal gases are always compressible
        if (phaseIdx == nPhaseIdx)
            return true;
        // the water component decides for the liquid phase...
        return Brine::liquidIsCompressible();
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal gas.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealGas(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        // let the fluids decide
        if (phaseIdx == nPhaseIdx)
            return Brine::gasIsIdeal() && CH4::gasIsIdeal();
        return false; // not a gas
    }

    /****************************************
     * Component related static parameters
     ****************************************/
    static const int numComponents = 8; // Brine, CH4, Acetate, Amendment, Methyl, H2
    static const int numMajorComponents = 2;// Brine, CH4

//    static const int H2OIdx       = wCompIdx;//0
    static const int BrineIdx     = wCompIdx;
    static const int CH4Idx       = nCompIdx;//1
    static const int AcetateIdx   = 2;
    static const int AmendmentIdx = 3;
    static const int RMethylIdx   = 4;
    static const int H2Idx        = 5;
    static const int NaClIdx      = 6;
    static const int TCIdx        = 7;
    static const int PrimBacIdx   = numComponents;
    static const int SecBacIdx    = numComponents + 1;
    static const int AcetoArchIdx = numComponents + 2;
    static const int HydroArchIdx = numComponents + 3;
    static const int MethyArchIdx = numComponents + 4;
    static const int CCoalIdx     = numComponents + 5;

    /*!
     * \brief Return the human readable name of a component
     *
     * \param compIdx The index of the component to consider
     */
    static const char *componentName(int compIdx)
    {
        switch (compIdx)
        {
//        case H2OIdx: return H2O::name();
        case BrineIdx: return Brine::name();
        case CH4Idx: return CH4::name();

        case AcetateIdx:return Acetate::name();
        case AmendmentIdx:return Amendment::name();
        case RMethylIdx:return RMethyl::name();
        case H2Idx:return H2::name();
        case PrimBacIdx:return "PrimaryBacteria";
        case SecBacIdx:return "SecondaryBacteria";
        case AcetoArchIdx:return "AcetoclasticArchaea";
        case HydroArchIdx:return "HydrogenotrophicArchaea";
        case MethyArchIdx:return "MethylotrophicArchaea";
        case CCoalIdx:return CCoal::name();
        case NaClIdx:return NaCl::name();
        case TCIdx: return "TotalC";
//        case CO2Idx: return CO2::name();
//        case HCO3Idx: return HCO3::name();
//        case CO3Idx: return CO3::name();
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
    }

    /*!
     * \brief Return the molar mass of a component in \f$\mathrm{[kg/mol]}\f$.
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(int compIdx)
    {
        switch (compIdx)
        {
//        case H2OIdx: return H2O::molarMass();
        case BrineIdx: return H2O::molarMass(); //TODO change H20 to Brine
        case CH4Idx: return CH4::molarMass();

        case AcetateIdx:   return Acetate::molarMass();
        case AmendmentIdx: return Amendment::molarMass();
        case RMethylIdx:   return RMethyl::molarMass();
        case H2Idx:        return H2::molarMass();
        case PrimBacIdx:   return Biofilm::molarMass();
        case SecBacIdx:    return Biofilm::molarMass();
        case AcetoArchIdx: return Biofilm::molarMass();
        case HydroArchIdx: return Biofilm::molarMass();
        case MethyArchIdx: return Biofilm::molarMass();
        case CCoalIdx:     return CCoal::molarMass();
        case NaClIdx:      return NaCl::molarMass();
        case TCIdx:        return HCO3::molarMass();
//        case CO2Idx:       return CO2::molarMass();
//        case HCO3Idx:      return HCO3::molarMass();
//        case CO3Idx:       return CO3::molarMass();

        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
    }

    //TODO Add charge? also ai bi values?
    /*!
     * \brief Return the charge value of a component.
     */
//    static Scalar charge(int compIdx)
//    {
//        Scalar z = 0;
//        switch (compIdx) {
//        case BrineIdx: z = 0;break;
// add all 0?
//        case NaIdx: z = Na::charge();break;
//        case ClIdx: z = Cl::charge();break;
//        case CO2Idx: z = 0;break;
//        case HCO3Idx: z = HCO3::charge();break;
//        case CO3Idx: z = CO3::charge();break;
//        default:DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);break;
//        }
//        return z;
//    }

    /*!
     * \brief Return the mass density of the precipitate \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param phaseIdx The index of the precipitated phase to consider
     */
    static Scalar precipitateDensity(int phaseIdx)
    {
//        if(phaseIdx != sPhaseIdx) //commented out for stability, not sure why needed for MECBM
//            DUNE_THROW(Dune::InvalidStateException, "Invalid solid phase index " << sPhaseIdx);
//        return NaCl::density();
        switch(phaseIdx)
        {
//        case sPhaseIdx:
//            return NaCl::density();
        case PrimBacPhaseIdx:
            return Biofilm::density();
        case SecBacPhaseIdx:
            return Biofilm::density();
        case AcetoArchPhaseIdx:
            return Biofilm::density();
        case HydroArchPhaseIdx:
            return Biofilm::density();
        case MethyArchPhaseIdx:
            return Biofilm::density();
        case CCoalPhaseIdx:
            return CCoal::density();
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid solid phase index " << numPhases+1);

    }

    /*!
     * \brief Return the saturation vapor pressure of the liquid phase \f$\mathrm{[Pa]}\f$.
     *
     * \param Temperature temperature of the liquid phase
     * \param salinity salinity (salt mass fraction) of the liquid phase
     */
     static Scalar vaporPressure(Scalar Temperature, Scalar salinity) //Vapor pressure dependence on Osmosis
     {
       return vaporPressure_(Temperature,salinity);
     }

    /*!
     * \brief Return the salt specific heat capacity \f$\mathrm{[J/molK]}\f$.
     *
     * \param phaseIdx The index of the precipitated phase to consider
     */
     static Scalar precipitateHeatCapacity(int phaseIdx)
    {
        return NaCl::heatCapacity();
    }

    /*!
     * \brief Return the molar density of the precipitate \f$\mathrm{[mol/m^3]}\f$.
     *
     * \param phaseIdx The index of the precipitated phase to consider
     */
    static Scalar precipitateMolarDensity(int phaseIdx)
     {
        return precipitateDensity(phaseIdx)/molarMass(phaseIdx);
     }

    /****************************************
     * thermodynamic relations
     ****************************************/
    /*!
     * \brief Initialize the fluid system's static parameters generically
     *
     * If a tabulated H2O component is used, we do our best to create
     * tables that always work.
     */
    static void init()
    {
        init(/*tempMin=*/273.15,
             /*tempMax=*/305.15,
             /*numTemptempSteps=*/10,
             /*startPressure=*/1e4,
             /*endPressure=*/40e6,
             /*pressureSteps=*/200);

    }
   /*!
    * \brief Initialize the fluid system's static parameters using
    *        problem specific temperature and pressure ranges
    *
    * \param tempMin The minimum temperature used for tabulation of water \f$\mathrm{[K]}\f$
    * \param tempMax The maximum temperature used for tabulation of water \f$\mathrm{[K]}\f$
    * \param nTemp The number of ticks on the temperature axis of the  table of water
    * \param pressMin The minimum pressure used for tabulation of water \f$\mathrm{[Pa]}\f$
    * \param pressMax The maximum pressure used for tabulation of water \f$\mathrm{[Pa]}\f$
    * \param nPress The number of ticks on the pressure axis of the  table of water
    */

    static void init(Scalar tempMin, Scalar tempMax, unsigned nTemp,
                      Scalar pressMin, Scalar pressMax, unsigned nPress)
    {
        if (useComplexRelations)
            std::cout << "Using complex Brine-CH4 fluid system\n";
        else
            std::cout << "Using fast Brine-CH4 fluid system\n";

        if (Brine::isTabulated) {
            std::cout << "Initializing tables for the Brine fluid properties ("
                        << nTemp*nPress
                        << " entries).\n";

            Brine::init(tempMin, tempMax, nTemp,
                                pressMin, pressMax, nPress);
        }
        //TODO check H2O Tabulated implementation
//        else if (H2O::isTabulated) {
//            std::cout << "Initializing tables for the Brine fluid properties ("
//                        << nTemp*nPress
//                        << " entries).\n";
//            H2O::init(tempMin, tempMax, nTemp,
//                    pressMin, pressMax, nPress);
//        }
    }

    using Base::density;
     /*!
     * \brief Given a phase's composition, temperature, pressure, and
     *        the partial pressures of all components, return its
     *        density \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param fluidState the fluid state
     * \param phaseIdx index of the phase
     *
     * Equation given in:
     * - Batzle & Wang (1992) \cite batzle1992
     * - cited by: Bachu & Adams (2002)
     *   "Equations of State for basin geofluids" \cite adams2002
     */
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,
                          int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        switch (phaseIdx) {
            case wPhaseIdx:
//                std::cout<< fluidState.moleFraction(wPhaseIdx,BrineIdx)<< "molFrac Brine \n"
//                <<fluidState.moleFraction(wPhaseIdx, AcetateIdx) << "molFrac Acetate \n"
//                <<fluidState.moleFraction(wPhaseIdx, AmendmentIdx) << "molFrac Amendment \n"
//                <<fluidState.moleFraction(wPhaseIdx, RMethylIdx) << "molFrac RMethyl \n"
//                <<fluidState.massFraction(wPhaseIdx, NaClIdx)<< "massFrac NaCl \n"
//                <<fluidState.moleFraction(wPhaseIdx, NaClIdx)<< "molFrac NaCl \n"
//                <<fluidState.averageMolarMass(wPhaseIdx)<< "avgMolMass \n"
//                <<fluidState.averageMolarMass(nPhaseIdx)<< "avgMolMass \n"
//                <<std::endl;

                return liquidDensity_(temperature,pressure,
                        fluidState.moleFraction(wPhaseIdx,BrineIdx),
                        fluidState.moleFraction(wPhaseIdx, AcetateIdx),
                        fluidState.moleFraction(wPhaseIdx, AmendmentIdx),
                        fluidState.moleFraction(wPhaseIdx, RMethylIdx),
                        fluidState.massFraction(wPhaseIdx, NaClIdx));
//                return H2O::liquidDensity(temperature,pressure);

//                        fluidState.moleFraction(wPhaseIdx, TCIdx));
//                return Brine::liquidDensity(temperature,
//                              pressure,
//                              fluidState.massFraction(wPhaseIdx, NaClIdx));
                //TODO As soon as there are relevant concentrations impacting the density, add the
                //     respective component massfraction here as "salinity (g_NaCl/l) to the density calcuation!
                // default salinity is used: Sal = 0.1
            case nPhaseIdx:
                return gasDensity_(temperature,
                            pressure,
                            fluidState.moleFraction(nPhaseIdx, BrineIdx),
                            fluidState.moleFraction(nPhaseIdx, CH4Idx),
                            fluidState.moleFraction(nPhaseIdx, H2Idx));

            default:
                DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
            }

      }
//    static Scalar liquidDensity(const Scalar temperature,
//            const Scalar pressure,
//            const Scalar xwBrine,
//            const Scalar xwAcetate,
//            const Scalar xwCarAm,
//            const Scalar xwRMethyl,
//            const Scalar XwNaCl)
//    {
//        return liquidDensity_(temperature, pressure, xwBrine, xwAcetate, xwCarAm, xwRMethyl, XwNaCl);
//    }

    using Base::viscosity;
    /*!
     * \brief Calculate the dynamic viscosity of a fluid phase \f$\mathrm{[Pa*s]}\f$
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     *
     * \note For the viscosity of the phases the contribution of the minor
     *       component is neglected. This contribution is probably not big, but somebody
     *       would have to find out its influence.
     */
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            int phaseIdx)
    {

        assert(0 <= phaseIdx && phaseIdx < numPhases);

        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);
        Scalar result = 0;

        if (phaseIdx == wPhaseIdx)
        {
             Scalar XNaCl = fluidState.massFraction(wPhaseIdx, NaClIdx);
             result = Brine::liquidViscosity(temperature, pressure, XNaCl);
                //TODO As soon as there are relevant concentrations impacting the viscosity, add the
                //     respective component massfraction here as "salinity (g_NaCl/l) to the calcuation!
                // default salinity is used: Sal = 0.1
//             result = H2O::liquidViscosity(temperature, pressure);
        }
        else if (phaseIdx == nPhaseIdx)
        {
            result = CH4::gasViscosity(temperature, pressure);
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);

        Valgrind::CheckDefined(result);
        return result;
    }

    using Base::fugacityCoefficient;
    /*!
     * \brief Returns the fugacity coefficient \f$\mathrm{[-]}\f$ of a component in a
     *        phase.
     * \param  fluidState The fluid state
     * \param phaseIdx Index of the phase
     * \param compIdx Index of the component
     *
     * The fugacity coefficient \f$\mathrm{\phi^\kappa_\alpha}\f$ of
     * component \f$\mathrm{\kappa}\f$ in phase \f$\mathrm{\alpha}\f$ is connected to
     * the fugacity \f$\mathrm{f^\kappa_\alpha}\f$ and the component's mole
     * fraction \f$\mathrm{x^\kappa_\alpha}\f$ by means of the relation
     *
     * \f[
     f^\kappa_\alpha = \phi^\kappa_\alpha\;x^\kappa_\alpha\;p_\alpha
     \f]
     * where \f$\mathrm{p_\alpha}\f$ is the pressure of the fluid phase.
     *
     * For liquids with very low miscibility this boils down to the
     * inverse Henry constant for the solutes and the saturated vapor pressure
     * both divided by phase pressure.
     */
    template <class FluidState>
    static Scalar fugacityCoefficient(const FluidState &fluidState, // TODO check if everything is working ok
                                      int phaseIdx,
                                      int compIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        assert(0 <= compIdx && compIdx < numComponents);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);
        assert(T > 0);
        assert(p > 0);

        if (phaseIdx == nPhaseIdx)
            return 1.0;

        else if (phaseIdx == wPhaseIdx)
        {
            std::cout<< Brine_CH4::henry(T)/p << "fug BrineCh4" <<std::endl;
        if (compIdx == BrineIdx)
            return Brine::vaporPressure(T)/p;
        else if (compIdx == CH4Idx)
            return Brine_CH4::henry(T)/p;
        //TODO Brine_CH4 henry: add salinity
        else if (compIdx == H2Idx)
            return H2O_H2::henry(T)/p; //TODO Check henry for salinity / create new brine_h2.hh?
//        else if (compIdx == TCIdx) //TODO change once CO2Tables are added correctly
//            return Brine_CO2::fugactiyCoefficientCO2(T, p);
        else
            return 1/(p);
        }
        else
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }


    using Base::diffusionCoefficient;
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState &fluidState,
                                       int phaseIdx,
                                       int compIdx)
    {
        DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients");
    }

    using Base::binaryDiffusionCoefficient;
    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return the binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for components
     *        \f$\mathrm{i}\f$ and \f$\mathrm{j}\f$ in this phase.
     *
     * \param fluidState An arbitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx The index of the first component to consider
     * \param compJIdx The index of the second component to consider
     */
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        assert(0 <= compIIdx && compIIdx < numComponents);
        assert(0 <= compJIdx && compJIdx < numComponents);

        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);

        if (phaseIdx == wPhaseIdx) { //TODO: Check values below! All arbitrary atm, but range is ok and shouldn't make any difference.
            Scalar result = 0.0;
            if(compJIdx == CH4Idx)
                result = Brine_CH4::liquidDiffCoeff(temperature, pressure);
            else if (compJIdx == AcetateIdx)
                result = 0.67e-9; // glucose value from internet //TODO ACTUAL SOURCE GOES HERE!
            else if (compJIdx == AmendmentIdx)
                result = 0.67e-9; // arbitrary value
            else if (compJIdx == RMethylIdx)
                result = 0.67e-9; // arbitrary value
            else if (compJIdx == H2Idx)
                result = H2O_H2::liquidDiffCoeff(temperature, pressure);
            else if (compJIdx == NaClIdx)
                result = 1.587e-9;  //[m²/s]    //value for NaCl; J. Phys. D: Appl. Phys. 40 (2007) 2769-2776
            else if (compJIdx == TCIdx)
                result = HCO3::liquidDiffCoeff(temperature, pressure);

            else
                DUNE_THROW(Dune::NotImplemented, "Binary diffusion coefficient of components "
                                                 << compIIdx << " and " << compJIdx
                                                 << " in phase " << phaseIdx);
            Valgrind::CheckDefined(result);
            return result;
        }
        else {
            assert(phaseIdx == nPhaseIdx);

            if (compIIdx != CH4Idx)
            std::swap(compIIdx, compJIdx);

            Scalar result = 0.0;
            if(compJIdx == BrineIdx)
                result = Brine_CH4::gasDiffCoeff(temperature, pressure);
            else if (compJIdx == AcetateIdx)
                result = 0;
            else if (compJIdx == AmendmentIdx)
                result = 0;
            else if (compJIdx == RMethylIdx)
                result = 0;
            else if (compJIdx == H2Idx)
                result = H2O_H2::gasDiffCoeff(temperature, pressure);
            else if (compJIdx == NaClIdx)
                result = 0;
            else if (compJIdx == TCIdx)
                result = 0; //TODO change once CO2Tables are added correctly
            else
                DUNE_THROW(Dune::NotImplemented, "Binary diffusion coefficient of components "
                                                 << compIIdx << " and " << compJIdx
                                                 << " in phase " << phaseIdx);
            Valgrind::CheckDefined(result);
            return result;
        }
    }

    using Base::enthalpy;
    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return its specific enthalpy \f$\mathrm{[J/kg]}\f$.
     * \param fluidState The fluid state
     * \param phaseIdx The index of the phase
     *
     * See:
     * Class 2000
     * Theorie und numerische Modellierung nichtisothermer Mehrphasenprozesse in NAPL-kontaminierten porösen Medien
     * Chapter 2.1.13 Innere Energie, Wäremekapazität, Enthalpie \cite A3:class:2001
     *
     * Formula (2.42):
     * the specific enthalpy of a gas phase result from the sum of (enthalpies*mass fraction) of the components
     * For the calculation of enthalpy of brine we refer to (Michaelides 1981)
     *
     * \note For the phase enthalpy the contribution of gas-molecules in the liquid phase
     *       is neglected. This contribution is probably not big. Somebody would have to
     *       find out the enthalpy of solution for this system. ...
     */
    template <class FluidState>
    static Scalar enthalpy(const FluidState &fluidState,
                           int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        if (phaseIdx == wPhaseIdx)
        {
            Scalar XlNaCl = fluidState.massFraction(phaseIdx, NaClIdx);
            Scalar result = Brine::liquidEnthalpy(T, p, XlNaCl);
                Valgrind::CheckDefined(result);
                return result;
        }
        else
        {
            Scalar XCH4 = fluidState.massFraction(nPhaseIdx, CH4Idx);
            Scalar XH2O = fluidState.massFraction(nPhaseIdx, BrineIdx);
//            Scalar XTC = fluidState.massFraction(nPhaseIdx, TCIdx); //TODO change once CO2Tables are added correctly

            Scalar result = 0;
            result += XH2O * H2O::gasEnthalpy(T, p);
            result += XCH4 * CH4::gasEnthalpy(T, p);
//            result += XTC * CO2::gasEnthalpy(T, p); //TODO change once CO2Tables are added correctly
            Valgrind::CheckDefined(result);
            return result;
        }
    }

    /*!
    * \brief Returns the specific enthalpy \f$\mathrm{[J/kg]}\f$ of a component in a specific phase
    * \param fluidState The fluid state
    * \param phaseIdx The index of the phase
    * \param componentIdx The index of the component
    */
    template <class FluidState>
    static Scalar componentEnthalpy(const FluidState &fluidState,
                                    int phaseIdx,
                                    int componentIdx)
    {
        Scalar T = fluidState.temperature(nPhaseIdx);
        Scalar p = fluidState.pressure(nPhaseIdx);
        Valgrind::CheckDefined(T);
        Valgrind::CheckDefined(p);

        if (phaseIdx == wPhaseIdx)
        {
            DUNE_THROW(Dune::NotImplemented, "The component enthalpies in the liquid phase are not implemented.");
        }
        else if (phaseIdx == nPhaseIdx)
        {
            if (componentIdx ==  BrineIdx)
            {
                return Brine::gasEnthalpy(T, p);
            }
            else if (componentIdx == CH4Idx)
            {
                return CH4::gasEnthalpy(T, p);
            }
//            else if (componentIdx == TCIdx) // change once CO2Tables are added correctly
//                       {
//                           return CO2::gasEnthalpy(T, p);
//                       }
            DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << componentIdx);
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }


    using Base::thermalConductivity;
    /*!
     * \brief Thermal conductivity of a fluid phase \f$\mathrm{[W/(m K)]}\f$.
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     *
     * \note For the thermal conductivity of the phases the contribution of the minor
     *       component is neglected. This contribution is probably not big, but somebody
     *       would have to find out its influence.
     */
    template <class FluidState>
    static Scalar thermalConductivity(const FluidState &fluidState,
                                      int phaseIdx)
    {
        if (phaseIdx == wPhaseIdx)
            return Brine::liquidThermalConductivity(fluidState.temperature(phaseIdx),
                                                  fluidState.pressure(phaseIdx));
        else // gas phase
            return CH4::gasThermalConductivity(fluidState.temperature(phaseIdx),
                                               fluidState.pressure(phaseIdx));
    }

    using Base::heatCapacity;
    /*!
     * \brief Specific isobaric heat capacity of a fluid phase.
     *        \f$\mathrm{[J/(kg*K)}\f$.
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     *
     * \note The calculation of the isobaric heat capacity is preliminary. A better
     *       description of the influence of the composition on the phase property
     *       has to be found.
     */
    template <class FluidState>
    static Scalar heatCapacity(const FluidState &fluidState,
                               int phaseIdx)
    {
        const Scalar temperature  = fluidState.temperature(phaseIdx);
        const Scalar pressure = fluidState.pressure(phaseIdx);
        if (phaseIdx == wPhaseIdx)
        {
            return Brine::liquidHeatCapacity(temperature, pressure);
        }
        else if (phaseIdx == nPhaseIdx)
        {
            return CH4::gasHeatCapacity(temperature, pressure) * fluidState.moleFraction(nPhaseIdx, CH4Idx)
                   + Brine::gasHeatCapacity(temperature, pressure) * fluidState.moleFraction(nPhaseIdx, BrineIdx);
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }
    /*!
     * \brief Return the molality of NaCl \f$\mathrm{[mol/m^3]}\f$.
     * \param fluidState An abitrary fluid state
     * \param paramCache parameter cache
     * \param salinity Salinity of brine
     */
    template <class FluidState>
    static Scalar molalityNaCl(const FluidState &fluidState,
                               const ParameterCache &paramCache,
                               Scalar salinity)
      {
        return Brine_CH4::molalityNaCl(salinity);
      }

private:
    //TODO correct implementation of liquidDensity_ regarding all relevant components
    static Scalar liquidDensity_(Scalar T,
            Scalar pw,
            Scalar xwBrine,
            Scalar xwAcetate,
            Scalar xwCarAm,
            Scalar xwRMethyl,
            Scalar XwNaCl)
    {
        Valgrind::CheckDefined(T);
        Valgrind::CheckDefined(pw);
        Valgrind::CheckDefined(xwBrine);

        if(T < 273.15) {
            DUNE_THROW(NumericalProblem,
                       "Liquid density for Brine and CO2 is only "
                       "defined above 273.15K (is" << T << ")");
        }
        if(pw >= 2.5e8) {
            DUNE_THROW(NumericalProblem,
                       "Liquid density for Brine and CO2 is only "
                       "defined below 250MPa (is" << pw << ")");
        }
        Scalar rho_brine = Brine::liquidDensity(T, pw, XwNaCl);
        Scalar rho_pure = H2O::liquidDensity(T, pw);
        Scalar rho_wAcetate = rho_pure; //should be somehting where rho is calculated
        Scalar contribAcetate = rho_wAcetate - rho_pure;
        return rho_brine + contribAcetate;
    }
    static Scalar gasDensity_(Scalar T, Scalar pg, Scalar xnBrine, Scalar xnCH4, Scalar xnH2)
    {
        //Dalton' Law
        const Scalar pBrine = xnBrine*pg;
        const Scalar pH2 = xnH2*pg;
        const Scalar pCH4 = pg - pBrine - pH2;
        const Scalar gasDensityCH4 = CH4::gasDensity(T, pCH4);
        const Scalar gasDensityH2 = H2::gasDensity(T, pH2);
        const Scalar gasDensityH2O = H2O::gasDensity(T, pBrine);
//        const Scalar gasDensityCO2 = CO2::gasDensity(T,pCO2);
        const Scalar gasDensity = gasDensityCH4 + gasDensityH2 + gasDensityH2O;// + gasDensityCO2;
        return gasDensity;
    }
};

} // end namespace
} // end namespace

#endif
